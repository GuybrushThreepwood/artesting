
/*===================================================================
	File: CameraUtils.cpp
=====================================================================*/

#include <opencv2/core/opengl.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "CameraUtils.h"

namespace
{

}


#ifdef __APPLE__
@implementation iOSCamera

-(id) init
{
    self = [super init];
    
    return self;
}

-(id) initWithClass:(util::CameraOutput *)camClass
{
    self = [super init];
    if( self != nil )
    {
        otherClass = camClass;
    }
    return self;
}

void fourCCStringFromCode(int code, char fourCC[5]) {
    for (int i = 0; i < 4; i++) {
        fourCC[3 - i] = code >> (i * 8);
    }
    fourCC[4] = '\0';
}

-(CGSize) getVideoSize
{
    return videoSize;
}

// Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
            return device;
    }
    return nil;
}

- (AVCaptureDevice *) microphoneWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
            return device;
    }
    return nil;
}

-(void) start
{
    if( camSession != nil )
    {
        [camSession commitConfiguration];
        [camSession startRunning];
    }
}

-(void) stop
{
    if( camSession != nil )
    {
        [camSession stopRunning];
        camSession = nil;
    }
}

-(void) createSession:(NSString*)preset cameraPosition:(AVCaptureDevicePosition)position framerate:(int)framerate
{
    if( camSession != nil )
    {
        [camSession stopRunning];
        camSession = nil;
    }
    
    camSession = [[AVCaptureSession alloc] init];
    if( [camSession canSetSessionPreset:preset] )
        camSession.sessionPreset = preset/*AVCaptureSessionPresetMedium*/;
    else
        camSession.sessionPreset = AVCaptureSessionPresetMedium;
            
    // video
    AVCaptureDevice *device = [self cameraWithPosition:position];//[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if(device != nil)
    {
        // set framerate
        [device lockForConfiguration:nil];
        //NSArray* framerates = [[device activeFormat] videoSupportedFrameRateRanges];
        //NSLog( @"Framerate = %@", framerates);
        [device setActiveVideoMinFrameDuration:CMTimeMake(1, framerate)];
        [device setActiveVideoMaxFrameDuration:CMTimeMake(1, framerate)];
        [device unlockForConfiguration];
        
        NSError *error = nil;
        
        camDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
        if (!camDeviceInput) {
            
            // Handle the error appropriately.
            NSLog(@"ERROR");
        }
        
        [camSession addInput:camDeviceInput];
        
        // create camera output
        vidDataOutput = [[AVCaptureVideoDataOutput alloc] init];
        
        if (!vidDataOutput) {
            
            // Handle the error appropriately.
            NSLog(@"ERROR");
        }
        
        // set output delegate to self
        dispatch_queue_t queue = dispatch_queue_create("vid_output_queue", NULL);
        [vidDataOutput setSampleBufferDelegate:self queue:queue];
        //dispatch_release(queue);
        
        // get best output video format
        NSArray *outputPixelFormats = vidDataOutput.availableVideoCVPixelFormatTypes;
        int bestPixelFormatCode = -1;
        for (NSNumber *format in outputPixelFormats) {
            int code = [format intValue];
            if (bestPixelFormatCode == -1)
                bestPixelFormatCode = code;  // choose the first as best
                char fourCC[5];
            fourCCStringFromCode(code, fourCC);
            NSLog(@"available video output format: %s (code %d)", fourCC, code);
        }
        
        // specify output video format
        //NSDictionary *outputSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:bestPixelFormatCode]
        //                                                           forKey:(id)kCVPixelBufferPixelFormatTypeKey];
        
        NSDictionary *outputSettings = @{ (id)kCVPixelBufferPixelFormatTypeKey : [NSNumber numberWithInteger:kCVPixelFormatType_32BGRA]};
        [vidDataOutput setVideoSettings:outputSettings];

        [camSession addOutput:vidDataOutput];
        
        // get the res
        AVCaptureDeviceFormat *activeFormat = [device activeFormat];
        if( activeFormat != nil )
        {
            CMVideoDimensions dims = CMVideoFormatDescriptionGetDimensions(activeFormat.formatDescription);
            
            videoSize = CGSizeMake(dims.width, dims.height);
            NSLog(@"Video format %dx%d", dims.width, dims.height);
        }
        
        // audio
        /*device = [self microphoneWithPosition:AVCaptureDevicePositionUnspecified];
        if(device != nil)
        {
            //[device lockForConfiguration:nil];
            //[device unlockForConfiguration];
            
            input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
            if (!input) {
                
                // Handle the error appropriately.
                NSLog(@"ERROR");
            }
            
            [camSession addInput:input];
        }*/
        
        // preview output
        /*previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:videoSession];
        
        [[previewLayer connection] setAutomaticallyAdjustsVideoMirroring:NO];
        [[previewLayer connection] setVideoMirrored:DataManager::GetInstance()->GetOptions()->cameraMirror];
        
        //previewLayer.orientation = AVCaptureVideoOrientationLandscapeRight;
        [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];*/
    }
}

#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate methods

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
fromConnection:(AVCaptureConnection *)connection
{
    if( otherClass == nil )
        return;
    
    if( !otherClass->CanReadImage() )
    {
        CVImageBufferRef imgBuf = CMSampleBufferGetImageBuffer(sampleBuffer);
        
        CVPixelBufferLockBaseAddress( imgBuf, 0 );
            //size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imgBuf);
            int bufferWidth = CVPixelBufferGetWidth(imgBuf);
            int bufferHeight = CVPixelBufferGetHeight(imgBuf);
            uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imgBuf);
            
            cv::Mat image = cv::Mat(bufferHeight,bufferWidth,CV_8UC4,(void*)baseAddress); //put buffer in open cv, no memory copied
            
            if( std::strlen((const char*)baseAddress) > 0 )
            {
                //Processing here
                otherClass->SetReadImage(image);
            }
            //End processing
        CVPixelBufferUnlockBaseAddress( imgBuf, 0 );
            
        otherClass->SetFrameUpdate(true);
    }
    
    /*CVImageBufferRef imgBuf = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // lock the buffer
    CVPixelBufferLockBaseAddress(imgBuf, 0);
    
    // get the address to the image data
    //    void *imgBufAddr = CVPixelBufferGetBaseAddress(imgBuf);   // this is wrong! see http://stackoverflow.com/a/4109153
    void *imgBufAddr = CVPixelBufferGetBaseAddressOfPlane(imgBuf, 0);
    
    // get image properties
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imgBuf);
    int w = (int)CVPixelBufferGetWidth(imgBuf);
    int h = (int)CVPixelBufferGetHeight(imgBuf);
    
    // create the cv mat
    mat.create(h, w, CV_8UC1);              // 8 bit unsigned chars for grayscale data
    memcpy(mat.data, imgBufAddr, w * h);    // the first plane contains the grayscale data
    // therefore we use <imgBufAddr> as source
    
    // unlock again
    CVPixelBufferUnlockBaseAddress(imgBuf, 0);*/
}

@end
#endif // __APPLE__

using namespace util;

CameraOutput::CameraOutput()
{
    m_RunCapture = true;
    m_CaptureEnded = false;
    m_VideoFrameUpdate = false;
}

CameraOutput::~CameraOutput()
{
    Release();
}

void CameraOutput::Release()
{
    Stop();
    
    if( m_VideoCapturer.isOpened() )
        m_VideoCapturer.release();
}

void CameraOutput::Create(const glm::ivec2& dimensions, int framerate, int cameraPosition )
{
#ifdef __APPLE__
    m_iOSCamera = [[iOSCamera alloc] initWithClass:this];
    
    [m_iOSCamera createSession:AVCaptureSessionPresetHigh cameraPosition:AVCaptureDevicePositionFront framerate:framerate];
    
    CGSize videoSize = [m_iOSCamera getVideoSize];
    m_ReadImage.create((int)videoSize.height, (int)videoSize.width, CV_8UC3);

	m_ViewSize = glm::ivec2( m_ReadImage.size().width, m_ReadImage.size().height);
#else
    // read from camera
    m_VideoCapturer.open(0);
    
    // check video is open
    if (!m_VideoCapturer.isOpened())
    {
        DBGLOG("ERROR: camera not opened");
        return;
    }
    
    m_VideoCapturer.set(CV_CAP_PROP_FPS, framerate);
    m_VideoCapturer.set(CV_CAP_PROP_FRAME_WIDTH, dimensions.x);
    m_VideoCapturer.set(CV_CAP_PROP_FRAME_HEIGHT, dimensions.y);
    
    m_VideoCapturer >> m_ReadImage;
    
    m_ViewSize = glm::ivec2( m_ReadImage.size().width, m_ReadImage.size().height);
    //m_ReadImage.create(m_ReadImage.size().width, m_ReadImage.size().height, CV_8UC3);
    
    m_Framerate = static_cast<int>( m_VideoCapturer.get(CV_CAP_PROP_FPS) );
    
#endif // __APPLE__
}

void CameraOutput::Start()
{
#ifdef __APPLE__
    [m_iOSCamera start];
#else
    m_RunCapture = true;
    m_CaptureEnded = false;
    m_VideoFrameUpdate = false;
    m_CaptureThread = std::thread(&CameraOutput::CaptureFrameThread, this);
    //m_CaptureThread.join();
    m_CaptureThread.detach();
#endif
}

void CameraOutput::Stop()
{
#ifdef __APPLE__
    [m_iOSCamera stop];
#else
    // stop the thread
    m_RunCapture = false;
    while (!m_CaptureEnded)
    {
        SDL_Delay(1);
    }
#endif
}

/////////////////////////////////////////////////////
/// Method: CaptureFrameThread
/// Params: None
///
/////////////////////////////////////////////////////
void CameraOutput::CaptureFrameThread()
{
    while (m_RunCapture)
    {
        m_CaptureMutex.lock();
        if (!m_VideoFrameUpdate)
        {
            m_VideoCapturer.read(m_ReadImage);
            
            m_VideoFrameUpdate = true;
        }
        m_CaptureMutex.unlock();
        
		SDL_Delay(m_Framerate);
    }
    
    m_CaptureEnded = true;
}
