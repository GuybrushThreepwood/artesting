
#ifndef __CAMERAUTILS_H__
#define __CAMERAUTILS_H__

#include "Boot/Includes.h"

#ifdef __APPLE__

namespace util { class CameraOutput; }

@interface iOSCamera : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate>
{
    AVCaptureSession *camSession;               // controlls the camera session
    AVCaptureDeviceInput *camDeviceInput;       // input device: camera
    AVCaptureVideoDataOutput *vidDataOutput;    // controlls the video output
    
    util::CameraOutput* otherClass;
    CGSize videoSize;
    
    cv::Mat mat;
}

-(id) initWithClass:(util::CameraOutput *)camClass;
-(void) createSession:(NSString*)preset cameraPosition:(AVCaptureDevicePosition)position framerate:(int)framerate;
-(CGSize) getVideoSize;
@end
#endif // __APPLE__

namespace util
{
    class CameraOutput
    {
        public:
            CameraOutput();
            ~CameraOutput();
        
            void Create(const glm::ivec2& dimensions, int framerate, int cameraPosition);
            void Release();
        
            void Start();
            void Stop();
        
            void SetFrameUpdate( bool state ) { m_VideoFrameUpdate = state; }
            void ReadNextImage()            { m_VideoFrameUpdate = false; }
            bool CanReadImage()             { return m_VideoFrameUpdate; }
            cv::Mat& GetReadImage()         { return m_ReadImage; }
            const glm::ivec2& GetViewSize() { return m_ViewSize; }
            int GetFramerate()              { return m_Framerate; }
        
            void SetReadImage( cv::Mat& src ) { m_ReadImage = src.clone(); }

        private:
            void CaptureFrameThread();
        
        private:
            glm::ivec2 m_ViewSize;
            int m_Framerate;
        
            // cv style capture
            cv::VideoCapture m_VideoCapturer;
            cv::Mat m_ReadImage;
        
            std::mutex m_CaptureMutex;
            std::thread m_CaptureThread;
            bool m_RunCapture;
            bool m_CaptureEnded;
            bool m_VideoFrameUpdate;
        
#ifdef __APPLE__
            iOSCamera* m_iOSCamera;
#endif // __APPLE__
    };
    
} // namespace util

#endif // __CAMERAUTILS_H__
