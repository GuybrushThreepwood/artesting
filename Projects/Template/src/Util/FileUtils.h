
#ifndef __FILEUTILS_H__
#define __FILEUTILS_H__

namespace util
{

	void SplitPath(const char *path, char *drive, char *dir, char *fname, char *ext);

} // namespace util

#endif // __FILEUTILS_H__
