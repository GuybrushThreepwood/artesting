
#ifndef __MODELUTILS_H__
#define __MODELUTILS_H__

#include "Boot/Includes.h"

namespace util
{
	struct DrawObject {
		GLuint vao;		// vertex array object
		GLuint vbo;		// vertex buffer object
		int numTriangles;
	};

	bool LoadObjAndConvert(float bmin[3], float bmax[3], std::vector<util::DrawObject>& drawObjects, const char* filename);

} // namespace util

#endif // __MODELUTILS_H__
