
#ifndef __ARUCOUTILS_H__
#define __ARUCOUTILS_H__

namespace aruco { class CameraParameters; class Marker; }

namespace util
{
	void GetCameraProjectionMatrix(const aruco::CameraParameters& params, const cv::Size& orgImgSize, const cv::Size& size, float proj_matrix[16], float gnear, float gfar, bool invert);

	void GetMarkerModelViewMatrix(const aruco::Marker& m, float outMat[16]);

	void GetModelViewMatrix(float outMat[16], const cv::Mat &Rvec, const cv::Mat &Tvec);

} // namespace util

#endif // __ARUCOUTILS_H__
