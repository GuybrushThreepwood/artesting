
/*===================================================================
	File: ModelUtils.cpp
=====================================================================*/

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"

#include "Util/FileUtils.h"
#include "Util/ModelUtils.h"

void CalcNormal(float N[3], float v0[3], float v1[3], float v2[3]) {
	float v10[3];
	v10[0] = v1[0] - v0[0];
	v10[1] = v1[1] - v0[1];
	v10[2] = v1[2] - v0[2];

	float v20[3];
	v20[0] = v2[0] - v0[0];
	v20[1] = v2[1] - v0[1];
	v20[2] = v2[2] - v0[2];

	N[0] = v20[1] * v10[2] - v20[2] * v10[1];
	N[1] = v20[2] * v10[0] - v20[0] * v10[2];
	N[2] = v20[0] * v10[1] - v20[1] * v10[0];

	float len2 = N[0] * N[0] + N[1] * N[1] + N[2] * N[2];
	if (len2 > 0.0f) {
		float len = std::sqrt(len2);

		N[0] /= len;
		N[1] /= len;
	}
}

bool util::LoadObjAndConvert(float bmin[3], float bmax[3], std::vector<util::DrawObject>& drawObjects, const char* filename)
{
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;


	std::string err;
	char directory[256];
	char fname[256];
	SplitPath(filename, nullptr, directory, fname, nullptr);
	std::string mtlFile = std::string(directory) /*+ std::string(fname) + ".mtl"*/;

	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filename, mtlFile.c_str());
	if (!err.empty()) {
		std::cerr << err << std::endl;
	}

	if (!ret) {
		DBGLOG("Failed to load %s\n", filename);
		return false;
	}

	//printf("Parsing time: %d [ms]\n", tm.msec());

	DBGLOG("# of vertices  = %d\n", (int)(attrib.vertices.size()) / 3);
	DBGLOG("# of normals   = %d\n", (int)(attrib.normals.size()) / 3);
	DBGLOG("# of texcoords = %d\n", (int)(attrib.texcoords.size()) / 2);
	DBGLOG("# of materials = %d\n", (int)materials.size());
	DBGLOG("# of shapes    = %d\n", (int)shapes.size());

	bmin[0] = bmin[1] = bmin[2] = std::numeric_limits<float>::max();
	bmax[0] = bmax[1] = bmax[2] = -std::numeric_limits<float>::max();

	{
		for (size_t s = 0; s < shapes.size(); s++) {
			util::DrawObject o;
			std::vector<float> vb; // pos(3float), normal(3float), color(3float)
			for (size_t f = 0; f < shapes[s].mesh.indices.size() / 3; f++) {

				tinyobj::index_t idx0 = shapes[s].mesh.indices[3 * f + 0];
				tinyobj::index_t idx1 = shapes[s].mesh.indices[3 * f + 1];
				tinyobj::index_t idx2 = shapes[s].mesh.indices[3 * f + 2];

				float v[3][3];
				for (int k = 0; k < 3; k++) {
					int f0 = idx0.vertex_index;
					int f1 = idx1.vertex_index;
					int f2 = idx2.vertex_index;
					assert(f0 >= 0);
					assert(f1 >= 0);
					assert(f2 >= 0);

					v[0][k] = attrib.vertices[3 * f0 + k];
					v[1][k] = attrib.vertices[3 * f1 + k];
					v[2][k] = attrib.vertices[3 * f2 + k];
					bmin[k] = std::min(v[0][k], bmin[k]);
					bmin[k] = std::min(v[1][k], bmin[k]);
					bmin[k] = std::min(v[2][k], bmin[k]);
					bmax[k] = std::max(v[0][k], bmax[k]);
					bmax[k] = std::max(v[1][k], bmax[k]);
					bmax[k] = std::max(v[2][k], bmax[k]);
				}

				float n[3][3];

				if (attrib.normals.size() > 0) {
					int f0 = idx0.normal_index;
					int f1 = idx1.normal_index;
					int f2 = idx2.normal_index;
					assert(f0 >= 0);
					assert(f1 >= 0);
					assert(f2 >= 0);
					for (int k = 0; k < 3; k++) {
						n[0][k] = attrib.normals[3 * f0 + k];
						n[1][k] = attrib.normals[3 * f1 + k];
						n[2][k] = attrib.normals[3 * f2 + k];
					}
				}
				else {
					// compute geometric normal
					CalcNormal(n[0], v[0], v[1], v[2]);
					n[1][0] = n[0][0]; n[1][1] = n[0][1]; n[1][2] = n[0][2];
					n[2][0] = n[0][0]; n[2][1] = n[0][1]; n[2][2] = n[0][2];
				}

				for (int k = 0; k < 3; k++) {
					vb.push_back(v[k][0]);
					vb.push_back(v[k][1]);
					vb.push_back(v[k][2]);

					vb.push_back(n[k][0]);
					vb.push_back(n[k][1]);
					vb.push_back(n[k][2]);

					// Use normal as color.
					float c[4] = { n[k][0], n[k][1], n[k][2], 1.0f };
					float len2 = c[0] * c[0] + c[1] * c[1] + c[2] * c[2];
					if (len2 > 0.0f) {
						float len = std::sqrt(len2);

						c[0] /= len;
						c[1] /= len;
						c[2] /= len;
					}
					vb.push_back(c[0] * 0.5f + 0.5f);
					vb.push_back(c[1] * 0.5f + 0.5f);
					vb.push_back(c[2] * 0.5f + 0.5f);
					vb.push_back(c[3] /** 0.5f + 0.5f*/);
				}

			}

			o.vao = renderer::INVALID_OBJECT;
			o.vbo = renderer::INVALID_OBJECT;
			o.numTriangles = 0;
			if (vb.size() > 0) {

				glGenBuffers(1, &o.vbo);
				glBindBuffer(GL_ARRAY_BUFFER, o.vbo);
				glBufferData(GL_ARRAY_BUFFER, vb.size() * sizeof(float), &vb.at(0), GL_STATIC_DRAW);

				o.numTriangles = ((int)vb.size() / 10) / 3;

				DBGLOG("shape[%d] # of triangles = %d\n", static_cast<int>(s), o.numTriangles);
			}

			drawObjects.push_back(o);
		}
	}

	printf("bmin = %f, %f, %f\n", bmin[0], bmin[1], bmin[2]);
	printf("bmax = %f, %f, %f\n", bmax[0], bmax[1], bmax[2]);

	return true;
}
