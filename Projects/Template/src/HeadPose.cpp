
/*===================================================================
	File: HeadPose.cpp
=====================================================================*/

#include "Util/ModelUtils.h"

#include "HeadPose.h"

#ifndef DLIB_FRONTAL_FACE_DETECTOr_Hh_
	#include <dlib/image_processing/frontal_face_detector.h>
#endif

#ifndef DLIB_RENDER_FACE_DeTECTIONS_H_
	#include <dlib/image_processing/render_face_detections.h>
#endif

#ifndef DLIB_IMAGE_PROCESSInG_H_h_
	#include <dlib/image_processing.h>
#endif

#ifndef DLIB_OPEnCV_HEADER
	#include <dlib/opencv.h>
#endif

namespace
{
	glm::vec2 vertList[] =
	{
		glm::vec2(1.0f, 1.0f),
		glm::vec2(-1.0f, 1.0f),
		glm::vec2(1.0f, -1.0f),
		glm::vec2(-1.0f, -1.0f),
	};

	enum {
		POINT_LJAW=0,
		POINT_RJAW,
		POINT_LEYE,
		POINT_REYE,
		POINT_NOSE,
		POINT_LMOUTH,
		POINT_RMOUTH,
	};

	cv::Point3f fullModelPointList[] = {
		cv::Point3f(-81.370895f, 103.178543f, -48.257252f),	// l jaw (v 2011)
		cv::Point3f(77.675453f, 103.178543f, -50.515999f),		// r jaw (v 1138)

		cv::Point3f(-22.836367f, 103.932373f, 30.314053f),	// l eye (v 314)
		cv::Point3f(21.498482f, 103.932373f, 29.684422f),	// r eye (v 0)

		cv::Point3f(-0.196159f, 78.692406f, 63.289707f),	//nose (v 1879)

		cv::Point3f(-21.679396f, 47.649422f, 45.220989f),	// l mouth (v 1502)
		cv::Point3f(20.765305f, 47.649422f, 44.618191f),	// r mouth (v 695)	
	};

	dlib::full_object_detection shape;
	std::vector<dlib::full_object_detection> shapes;

	dlib::frontal_face_detector detector;
	dlib::shape_predictor sp;

	#define FACE_DOWNSAMPLE_RATIO 2
	#define SKIP_FRAMES 2

	cv::Mat im_small, im_display;
	int detectCount = 0;
	std::vector<dlib::rectangle> faces;

	double rot[9] = { 0 };
	std::vector<double> rv(3), tv(3);
	cv::Mat rotVec(rv), transVec(tv);

	bool showFaceOutline = true;
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
HeadPose::HeadPose(StateManager& stateManager)
: IState( stateManager, 0 )
{
	m_LastDelta = 0.0f;
	m_pPrimitivesDraw = nullptr;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
HeadPose::~HeadPose()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void HeadPose::Enter()
{
	// vsync on/off
	SDL_GL_SetSwapInterval(0);

	m_pPrimitivesDraw = new renderer::Primitives;
	m_pPrimitivesDraw->InitialisePrimitives();

	m_Camera = new util::CameraOutput;
	glm::ivec2 videoSize(640, 480);
	m_Camera->Create(videoSize, 30, 0);

	m_BGRImage.create(m_Camera->GetReadImage().size(), CV_8UC3);

	// make sure the first frame is the correct colour and orientation
	cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);
	cv::flip(m_BGRImage, m_BGRImage, 0);

	// We need a face detector.  We will use this to get bounding boxes for
	// each face in an image.
	detector = dlib::get_frontal_face_detector();
	// And we also need a shape_predictor.  This is the tool that will predict face
	// landmark positions given an image and face bounding box.  Here we are just
	// loading the model from the shape_predictor_68_face_landmarks.dat file you gave
	// as a command line argument.
	dlib::deserialize("data/shape_predictor_68_face_landmarks.dat") >> sp;

	// create texture for video 
	glGenTextures(1, &m_VideoTexture);
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));


	m_ModelShader.CreateFromFiles("data/shaders/21/model+colour.vert", "data/shaders/21/model+colour.frag", "");
	m_DrawObjects.clear();

	float bmin[3], bmax[3];
	if (!util::LoadObjAndConvert(bmin, bmax, m_DrawObjects, "data/models/head-obj-center.obj"))
	{
		DBGLOG("Model load failed\n");
	}

	m_ModelPoints3D.clear();
	m_ImagePoints2D.clear();

	//originalPoints = originalPoints - m;
	//assert(norm(mean(op)) < 1e-05); //make sure is centered
	//originalPoints = originalPoints + Scalar(0,0,25);
	//cout << "model points " << op << endl;

	rotVec = cv::Mat(rv);
	double _d[9] = { 1, 0, 0,
		0, -1, 0,
		0, 0, -1 };

	cv::Rodrigues(cv::Mat(3, 3, CV_64FC1, _d), rotVec);

	tv[0] = 0; tv[1] = 0; tv[2] = 1;
	transVec = cv::Mat(tv);

	m_CamMatrix = cv::Mat(3, 3, CV_64FC1);

	GL_CHECK

	m_Camera->Start();
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void HeadPose::Exit()
{
	m_Camera->Stop();
	delete m_Camera;

	if (m_pPrimitivesDraw != nullptr)
	{
		m_pPrimitivesDraw->ShutdownPrimitives();
		delete m_pPrimitivesDraw;
		m_pPrimitivesDraw = nullptr;
	}

	m_FBOFinal.Release();

	if (m_VideoTexture != renderer::INVALID_OBJECT)
		glDeleteTextures(1, &m_VideoTexture);

	m_ModelShader.Shutdown();
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int HeadPose::TransitionIn()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int HeadPose::TransitionOut()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void HeadPose::Update(float deltaTime)
{
	m_LastDelta = deltaTime;

	if (m_Camera->CanReadImage())
	{
		// Resize image for face detection
		cv::resize(m_Camera->GetReadImage(), im_small, cv::Size(), 1.0 / FACE_DOWNSAMPLE_RATIO, 1.0 / FACE_DOWNSAMPLE_RATIO);

		// Change to dlib's image format. No memory is copied.
		dlib::cv_image<dlib::bgr_pixel> cimg_small(im_small);
		dlib::cv_image<dlib::bgr_pixel> cimg(m_Camera->GetReadImage());

		// by deafult, opencv works in BGR, so we must convert to RGB because OpenGL in windows prefer
		cv::cvtColor(m_Camera->GetReadImage(), m_BGRImage, CV_BGR2RGB);

		cv::flip(m_BGRImage, m_BGRImage, 0);

		// Detect faces on resize image
		if (detectCount % SKIP_FRAMES == 0)
		{
			faces = detector(cimg_small);
		}
		detectCount++;

		// Find the pose of each face.
		for (unsigned long i = 0; i < faces.size(); ++i)
		{
			// Resize obtained rectangle for full resolution image. 
			dlib::rectangle r(
				(long)(faces[i].left() * FACE_DOWNSAMPLE_RATIO),
				(long)(faces[i].top() * FACE_DOWNSAMPLE_RATIO),
				(long)(faces[i].right() * FACE_DOWNSAMPLE_RATIO),
				(long)(faces[i].bottom() * FACE_DOWNSAMPLE_RATIO)
				);

			// Landmark detection on full sized image
			shape = sp(cimg, r);
			shapes.push_back(shape);
		}

		// collect 2D points
		// 0 - 16	// Jaw line
		// 17 - 21	// Left eyebrow
		// 22 - 26	// Right eyebrow
		// 27 - 30	// Nose bridge
		// 30 - 35	// Lower nose
		// 36 - 41	// Left eye
		// 42 - 47	// Right Eye
		// 48 - 59	// Outer lip
		// 60 - 67	// Inner lip

		//if (shape.num_parts() == 68)
		{
			m_ImagePoints2D.clear();
			m_ModelPoints3D.clear();

			if (shape.num_parts() > 0 && shape.num_parts() >= 16)
			{
				m_ImagePoints2D.push_back(cv::Point2f(shape.part(0).x(), shape.part(0).y())); // l jaw
				m_ModelPoints3D.push_back(fullModelPointList[POINT_LJAW]);	// l jaw (v 2011)

				m_ImagePoints2D.push_back(cv::Point2f(shape.part(16).x(), shape.part(16).y())); // r jaw
				m_ModelPoints3D.push_back(fullModelPointList[POINT_RJAW]);		// r jaw (v 1138)
			}

			if (shape.num_parts() > 30 && shape.num_parts() >= 35)
			{
				m_ImagePoints2D.push_back(cv::Point2f(shape.part(30).x(), shape.part(30).y())); // nose
				m_ModelPoints3D.push_back(fullModelPointList[POINT_NOSE]);	//nose (v 1879)
			}

			if (shape.num_parts() > 36 && shape.num_parts() >= 41)
			{				
				m_ImagePoints2D.push_back(cv::Point2f(shape.part(39).x(), shape.part(39).y())); // l eye
				m_ModelPoints3D.push_back(fullModelPointList[POINT_LEYE]);	// l eye (v 314)
			}

			if (shape.num_parts() > 42 && shape.num_parts() >= 47)
			{			
				m_ImagePoints2D.push_back(cv::Point2f(shape.part(42).x(), shape.part(42).y())); // r eye
				m_ModelPoints3D.push_back(fullModelPointList[POINT_REYE]);	// r eye (v 0)
			}

			if (shape.num_parts() > 48 && shape.num_parts() >= 59)
			{			
				m_ImagePoints2D.push_back(cv::Point2f(shape.part(48).x(), shape.part(48).y())); // l mouth
				m_ImagePoints2D.push_back(cv::Point2f(shape.part(54).x(), shape.part(54).y())); // r mouth

				m_ModelPoints3D.push_back(fullModelPointList[POINT_LMOUTH]);	// l mouth (v 1502)
				m_ModelPoints3D.push_back(fullModelPointList[POINT_RMOUTH]);	// r mouth (v 695)
			}


			m_ImagePointsMat = cv::Mat(m_ImagePoints2D);

			m_OriginalPointsMat = cv::Mat(m_ModelPoints3D);
			cv::Scalar m = cv::mean(cv::Mat(m_ModelPoints3D));

			if (m_ImagePoints2D.size() > 0 &&
				m_ModelPoints3D.size() > 0)
			{
				int max_d = std::max(m_Camera->GetReadImage().rows, m_Camera->GetReadImage().cols);
				m_CamMatrix = (cv::Mat_<double>(3, 3) << max_d, 0, m_Camera->GetReadImage().cols / 2.0,
					0, max_d, m_Camera->GetReadImage().rows / 2.0,
					0, 0, 1.0);
				//cout << "using cam matrix " << endl << camMatrix << endl;

				double _dc[] = { 0, 0, 0, 0 };
				cv::solvePnP(m_OriginalPointsMat, m_ImagePointsMat, m_CamMatrix, cv::Mat(1, 4, CV_64FC1, _dc), rotVec, transVec, false, CV_EPNP);

				cv::Mat rotM(3, 3, CV_64FC1, rot);
				cv::Rodrigues(rotVec, rotM);

				double* _r = rotM.ptr<double>();
				//printf("rotation mat: \n %.3f %.3f %.3f\n%.3f %.3f %.3f\n%.3f %.3f %.3f\n",
				//	_r[0], _r[1], _r[2], _r[3], _r[4], _r[5], _r[6], _r[7], _r[8]);
				//printf("trans vec: \n %.3f %.3f %.3f\n", tv[0], tv[1], tv[2]);

				double _pm[12] = { _r[0], _r[1], _r[2], tv[0],
					_r[3], _r[4], _r[5], tv[1],
					_r[6], _r[7], _r[8], tv[2] };

				cv::Matx34d P(_pm);
				cv::Mat KP = m_CamMatrix * cv::Mat(P);
				//	cout << "KP " << endl << KP << endl;

				//reproject object points - check validity of found projection matrix
				/*for (int i = 0; i<m_OriginalPointsMat.rows; i++) {
					cv::Mat_<double> X = (cv::Mat_<double>(4, 1) << m_OriginalPointsMat.at<float>(i, 0), m_OriginalPointsMat.at<float>(i, 1), m_OriginalPointsMat.at<float>(i, 2), 1.0);
					//		cout << "object point " << X << endl;
					cv::Mat_<double> opt_p = KP * X;
					cv::Point2f opt_p_img(opt_p(0) / opt_p(2), opt_p(1) / opt_p(2));
					//		cout << "object point reproj " << opt_p_img << endl; 

					//cv::circle(img, opt_p_img, 4, cv::Scalar(0, 0, 255), 1);
				}*/

				rotM = rotM.t();// transpose to conform with majorness of opengl matrix
			}
		}

		m_Camera->ReadNextImage();
	}
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void HeadPose::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	static float depthTweak = 150.0f;

	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_A]) {
		depthTweak += 100.0f*m_LastDelta;
		//DBGLOG("%.2f\n", depthTweak);

	}
	if (state[SDL_SCANCODE_Z]) {
		depthTweak -= 100.0f*m_LastDelta;
		//DBGLOG("%.2f\n", depthTweak);
	}
	if (state[SDL_SCANCODE_F]) {
		showFaceOutline = !showFaceOutline;
	}

	// start rendering
	DrawVideoToTexture();

	renderer::OpenGL::GetInstance()->DepthMode(true, GL_LESS);
	renderer::OpenGL::GetInstance()->SetNearFarClip(-1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(m_BGRImage.size().width, m_BGRImage.size().height, true);

    glm::mat4 mdlMat = glm::mat4(1.0f);
	renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);

	// Custom Face Render
	if (showFaceOutline)
		RenderFace(shape);

	//renderer::OpenGL::GetInstance()->SetNearFarClip(-1000.0f, 1000.0f);
	//renderer::OpenGL::GetInstance()->SetupOrthographicView(m_WindowDims.x, m_WindowDims.y, true);
	renderer::OpenGL::GetInstance()->SetNearFarClip(1.0f, 2000.0f);
	renderer::OpenGL::GetInstance()->SetupPerspectiveView(m_WindowDims.x, m_WindowDims.y, true);
    glm::mat4 viewMat = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.1f), glm::vec3(0.0f, 1.0f, 0.0f));
	renderer::OpenGL::GetInstance()->SetViewMatrix( viewMat );

	mdlMat = glm::translate(glm::mat4(1.0f), glm::vec3(tv[0], tv[1], tv[2] + depthTweak));

	float rotMat[16] = { (float)rot[0], (float)rot[1], (float)rot[2], 0.0f,
		(float)rot[3], (float)rot[4], (float)rot[5], 0.0f,
		(float)rot[6], (float)rot[7], (float)rot[8], 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f };
	glm::mat4 rotation = glm::make_mat4(rotMat);

	// rotate it
	mdlMat = mdlMat*rotation;

	renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);

	renderer::OpenGL::GetInstance()->DepthMode(true, GL_LESS);
	renderer::OpenGL::GetInstance()->DisableTexturing();
	DrawModels(m_DrawObjects);

	renderer::OpenGL::GetInstance()->DisableVBO();
}

/////////////////////////////////////////////////////
/// Method: Resize
/// Params: None
///
/////////////////////////////////////////////////////
int HeadPose::Resize(const glm::ivec2& windowSize)
{
	m_WindowDims = windowSize;

	renderer::OpenGL::GetInstance()->SetViewport(m_WindowDims.x, m_WindowDims.y);

	// create the FBO 
	m_FBOFinal.Release();

	m_FBOFinal.Initialise();
	m_FBOFinal.Create(m_WindowDims.x, m_WindowDims.y, (renderer::RTTFLAG_CREATE_COLOURBUFFER | renderer::RTTFLAG_CREATE_DEPTHBUFFER), GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, false);
	m_FBOFinal.SetClearColour(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f));

	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	// cache the uniforms and attributes
	m_TextureSampler = m_FBOFinal.GetUniformLocation(renderer::Shader::TexUnit0);
	if (m_TextureSampler != -1)
	glUniform1i(m_TextureSampler, 0);

	m_VertexAttr = m_FBOFinal.GetAttributeLocation(renderer::Shader::Vertex);

	renderer::OpenGL::GetInstance()->UseProgram(0);

	renderer::OpenGL::GetInstance()->SetPerspective(75.0f, 4.0f / 3.0f, 1.0f, 1000.0f);

	return 0;
}

/////////////////////////////////////////////////////
/// Method: DrawVideoToTexture
/// Params: None
///
/////////////////////////////////////////////////////
void HeadPose::DrawVideoToTexture()
{
	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	renderer::OpenGL::GetInstance()->DepthMode(false, GL_ALWAYS);
	renderer::OpenGL::GetInstance()->BlendMode(false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	renderer::OpenGL::GetInstance()->SetNearFarClip(-1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(m_WindowDims.x, m_WindowDims.y, true);

	renderer::OpenGL::GetInstance()->DisableVBO();

	// bind the video texture
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);

	// no PBO
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_BGR, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_RGBImage.size().width, m_RGBImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_RGBImage.ptr(0));
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_BGRImage.size().width, m_BGRImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	// render fullscreen quad with video texture
	if (m_VertexAttr != -1)
	{
		glEnableVertexAttribArray(m_VertexAttr);
		glVertexAttribPointer(m_VertexAttr, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &vertList[0]);
	}

	GL_CHECK

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (m_VertexAttr != -1)
	{
		glDisableVertexAttribArray(m_VertexAttr);
	}

	renderer::OpenGL::GetInstance()->UseProgram(0);

	GL_CHECK
}

/////////////////////////////////////////////////////
/// Method: DrawLine
/// Params: None
///
/////////////////////////////////////////////////////
void HeadPose::DrawLine(const dlib::full_object_detection& d, const int start, const int end, bool isClosed)
{
	/*std::vector <cv::Point> points;
	for (int i = start; i <= end; ++i)
	{
		points.push_back(cv::Point(d.part(i).x(), d.part(i).y()));
	}
	cv::polylines(img, points, isClosed, cv::Scalar(255, 0, 0), 2, 16);*/

	std::vector <glm::vec3> points;
	for (int i = start; i <= end; ++i)
	{
		points.push_back(glm::vec3(d.part(i).x(), float(m_BGRImage.size().height) - d.part(i).y(), 0.0f));
	}

	int total = end - start;
	float divider = 1.0f / (float)total;

	float colour = 1.0f;

	for (std::size_t i = 0; i < points.size(); ++i)
	{
		if (i != points.size()-1 )
			m_pPrimitivesDraw->DrawLine(points[i], points[i + 1], glm::vec4(0.0f, colour-=divider, 0.0f, 1.0f));
		else
		{
			if (isClosed)
			{
				m_pPrimitivesDraw->DrawLine(points[i], points[0], glm::vec4(0.0f, colour -= divider, 0.0f, 1.0f));
			}
		}
	}
}

/////////////////////////////////////////////////////
/// Method: RenderFace
/// Params: None
///
/////////////////////////////////////////////////////
void HeadPose::RenderFace(const dlib::full_object_detection& d)
{
	/*if (d.num_parts() != 68)
	{
		//DBGLOG("\n\t Invalid inputs were given to this function.\n\t d.num_parts():  %d", d.num_parts());
		return;
	}*/

	if (d.num_parts() > 0 && d.num_parts() >= 16 )
		DrawLine(d, 0, 16);           // Jaw line

	if (d.num_parts() > 17 && d.num_parts() >= 21)
		DrawLine(d, 17, 21);          // Left eyebrow

	if (d.num_parts() > 22 && d.num_parts() >= 26)
		DrawLine(d, 22, 26);          // Right eyebrow

	if (d.num_parts() > 27 && d.num_parts() >= 30)
		DrawLine(d, 27, 30);          // Nose bridge

	if (d.num_parts() > 30 && d.num_parts() >= 35)
		DrawLine(d, 30, 35, true);    // Lower nose

	if (d.num_parts() > 36 && d.num_parts() >= 41)
		DrawLine(d, 36, 41, true);    // Left eye

	if (d.num_parts() > 42 && d.num_parts() >= 47)
		DrawLine(d, 42, 47, true);    // Right Eye

	if (d.num_parts() > 48 && d.num_parts() >= 59)
		DrawLine(d, 48, 59, true);    // Outer lip

	if (d.num_parts() > 60 && d.num_parts() >= 67)
		DrawLine(d, 60, 67, true);    // Inner lip
}

/////////////////////////////////////////////////////
/// Method: DrawModels
/// Params: None
///
/////////////////////////////////////////////////////
void HeadPose::DrawModels(const std::vector<util::DrawObject>& drawObjects)
{
	m_ModelShader.Bind();

	GLint vertexAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Vertex);
	GLint colourAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Colour);
	GLint normalAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Normal);

	GLint modelViewProjectionMatrix = m_ModelShader.GetUniformLocation(renderer::Shader::ModelViewProjectionMatrix);

	glm::mat4 projMatrix = renderer::OpenGL::GetInstance()->GetProjectionMatrix();
	glm::mat4 viewMatrix = renderer::OpenGL::GetInstance()->GetViewMatrix();
	glm::mat4 modelMatrix = renderer::OpenGL::GetInstance()->GetModelMatrix();

	glm::mat4 modelViewMatrix = viewMatrix*modelMatrix;

	if (modelViewProjectionMatrix != -1)
		glUniformMatrix4fv(modelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix*modelViewMatrix));

	for (size_t i = 0; i < drawObjects.size(); i++) {
		util::DrawObject o = drawObjects[i];
		if (o.vbo == renderer::INVALID_OBJECT) {
			continue;
		}

		glBindBuffer(GL_ARRAY_BUFFER, o.vbo);

		if (vertexAttr != -1)
		{
			glEnableVertexAttribArray(vertexAttr);
			glVertexAttribPointer(vertexAttr, 3, GL_FLOAT, GL_FALSE, 40, (const void*)0); // 3*4 12 bytes 
		}

		if (normalAttr != -1)
		{
			glEnableVertexAttribArray(normalAttr);
			glVertexAttribPointer(normalAttr, 3, GL_FLOAT, GL_FALSE, 40, (const void*)(sizeof(float) * 3)); // 3*4 12 bytes
		}

		if (colourAttr != -1)
		{
			glEnableVertexAttribArray(colourAttr);
			glVertexAttribPointer(colourAttr, 4, GL_FLOAT, GL_FALSE, 40, (const void*)(sizeof(float) * 3)); // 4*4 16 bytes
		}

		glDrawArrays(GL_TRIANGLES, 0, 3 * o.numTriangles);

		GL_CHECK
	}

	if (vertexAttr != -1)
		glDisableVertexAttribArray(vertexAttr);

	if (normalAttr != -1)
		glDisableVertexAttribArray(normalAttr);

	if (colourAttr != -1)
		glDisableVertexAttribArray(colourAttr);

	m_ModelShader.UnBind();
}
