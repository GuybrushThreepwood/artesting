
/*===================================================================
	File: HeadPose.h
=====================================================================*/

#ifndef __HEADPOSE_H__
#define __HEADPOSE_H__

#include "Boot/Includes.h"

namespace util { struct DrawObject; }
namespace cv { class VideoCapture; class Mat; }
namespace dlib { class full_object_detection; }

class HeadPose : public IState
{
	public:
		HeadPose(StateManager& stateManager);
		virtual ~HeadPose();

		virtual void Enter();
		virtual void Exit();

		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Render();
		virtual int Resize(const glm::ivec2& windowSize);

	private:
		void DrawVideoToTexture();

		void DrawLine(const dlib::full_object_detection& d, const int start, const int end, bool isClosed = false);
		void RenderFace(const dlib::full_object_detection& d);

		void DrawModels(const std::vector<util::DrawObject>& drawObjects);

	private:
		float m_LastDelta;

		util::CameraOutput* m_Camera;
		cv::Mat m_BGRImage;

		GLuint m_VideoTexture;

		renderer::Framebuffer m_FBOFinal;
		GLint m_TextureSampler;
		GLint m_VertexAttr;

		renderer::Primitives* m_pPrimitivesDraw;

		renderer::Shader m_ModelShader;

		std::vector<cv::Point3f> m_ModelPoints3D;
		std::vector<cv::Point2f> m_ImagePoints2D;
		std::vector<util::DrawObject> m_DrawObjects;

		cv::Mat m_OriginalPointsMat;
		cv::Mat m_CamMatrix;

		cv::Mat m_ImagePointsMat;
};

#endif // __HEADPOSE_H__

