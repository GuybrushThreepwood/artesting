
/*===================================================================
	File: main.cpp
=====================================================================*/

//#include "DemoApp.h"
#include "DemoAppGLSL.h"

#ifdef USE_SDL_MAIN
int SDL_main(int argc, char *argv[])
#else
int main(int argc, char *argv[])
#endif
{
	DemoApp* pApp = new DemoApp(argc, argv);

	if (pApp != 0)
	{
		glm::ivec2 windowSize(1024, 768);
		glm::ivec2 frameSize(1024, 768);

		pApp->Execute(windowSize, frameSize, false);
	}

	delete pApp;

	return 0;
}