
/*===================================================================
	File: ArucoPlayground.h
=====================================================================*/

#ifndef __ARUCOPLAYGROUND_H__
#define __ARUCOPLAYGROUND_H__

#include "Boot/Includes.h"

// aruco 
#include "aruco.h"

class ArucoPlayground : public IState
{
	public:
		ArucoPlayground(StateManager& stateManager);
		virtual ~ArucoPlayground();

		virtual void Enter();
		virtual void Exit();

		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Render();
		virtual int Resize(const glm::ivec2& windowSize);

	private:
		void DrawVideoToTexture();

	private:
		float m_LastDelta;

		util::CameraOutput* m_Camera;
		cv::Mat m_BGRImage, m_RGBImage;

		aruco::CameraParameters m_CameraParameters;

		aruco::MarkerDetector m_PPDetector;
		std::vector<aruco::Marker> m_Markers;

		GLuint m_VideoTexture;

		renderer::Framebuffer m_FBOFinal;
		GLint m_TextureSampler;
		GLint m_VertexAttr;

		float m_ArucoProjMat[16];
		float m_ArucoMdlViewMat[16];
		renderer::Primitives* m_pPrimitivesDraw;
};

#endif // __ARUCOPLAYGROUND_H__

