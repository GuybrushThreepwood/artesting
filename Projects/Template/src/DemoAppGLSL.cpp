
/*===================================================================
	File: DemopApp.cpp
=====================================================================*/

#include "Boot/Includes.h"

#include "ArucoPlayground.h"
#include "DlibPlayground.h"
#include "ModelViewer.h"
#include "HeadPose.h"
#include "ArucoModel.h"
#include "CameraOutputTest.h"
#include "FaceTracker.h"
#include "DemoAppGLSL.h"

namespace
{
}

DemoApp::DemoApp()
{

}

DemoApp::DemoApp(int &argc, char **argv )
	: App(argc, argv)
{

}

DemoApp::~DemoApp()
{

}

int DemoApp::Initialise()
{
	renderer::InititaliseFBOShader();

	// camera output test
	if (0)
	{
		m_MainStateManager.ChangeState(new CameraOutputTest(m_MainStateManager));

		glm::ivec2 windowSize(1024, 768);
		ResizeWindow(windowSize);
	}

	// model viewer
	if (0)
	{
		m_MainStateManager.ChangeState(new ModelViewer(m_MainStateManager));

		glm::ivec2 windowSize(1024, 768);
		ResizeWindow(windowSize);
	}

	//aruco playground
	if (0)
	{
		// load the first state
		m_MainStateManager.ChangeState(new ArucoPlayground(m_MainStateManager));

		glm::ivec2 windowSize(1024,768);
		ResizeWindow(windowSize);
	}

	// aruco marker test
	if (1)
	{
		m_MainStateManager.ChangeState(new ArucoModel(m_MainStateManager));

		glm::ivec2 windowSize(1024, 768);
		ResizeWindow(windowSize);
	}

	// dlib
	if (0)
	{
		m_MainStateManager.ChangeState(new DlibPlayground(m_MainStateManager));

		glm::ivec2 windowSize(1024, 768);
		ResizeWindow(windowSize);
	}

	// head pose
	if (0)
	{
		m_MainStateManager.ChangeState(new HeadPose(m_MainStateManager));

		glm::ivec2 windowSize(1024, 768);
		ResizeWindow(windowSize);
	}
    
	//face tracker
	if (0)
	{
		m_MainStateManager.ChangeState(new FaceTracker(m_MainStateManager));

		glm::ivec2 windowSize(640, 480);
		ResizeWindow(windowSize);
	}

	return 0;
}

int DemoApp::Resize(const glm::ivec2& windowSize)
{
	glm::ivec2 size = windowSize;

	/*if (size.x * 3 % 4 != 0) {
		DBGLOG("DemoApp::Resize: changing width to avoid padding in cv");
		size.x += size.x * 3 % 4; //resize to avoid padding
		Resize(size);
	}*/

	// pass the size
	m_MainStateManager.Resize(size);

	return 0;
}

int DemoApp::Update()
{
	m_MainStateManager.Update(m_ElapsedTime);

	return 0;
}

int DemoApp::Render()
{
	m_MainStateManager.Render();

	return 0;
}

int DemoApp::Shutdown()
{
	m_MainStateManager.ChangeState(nullptr);

	return 0;
}


