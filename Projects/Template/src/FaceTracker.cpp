
/*===================================================================
	File: FaceTracker.cpp
=====================================================================*/

#include <FaceTracker/Tracker.h>

#include "FaceTracker.h"

namespace
{
	glm::vec2 vertList[] =
	{
		glm::vec2(1.0f, 1.0f),
		glm::vec2(-1.0f, 1.0f),
		glm::vec2(1.0f, -1.0f),
		glm::vec2(-1.0f, -1.0f),
	};

	std::string ftFile("data/facetracker/model/face2.tracker");
	std::string conFile("data/facetracker/model/face.con");
	std::string triFile("data/facetracker/model/face.tri");

	//set other tracking parameters
	std::vector<int> wSize1(1);
	std::vector<int> wSize2(3);
	int nIter = 5; double clamp = 3, fTol = 0.01;
	FACETRACKER::Tracker model;
	cv::Mat tri;
	cv::Mat con;

	bool failed = true;

	bool fcheck = false; 
	double scale = 1; 
	int fpd = -1; 
	bool show = true;
}

void Draw(cv::Mat &image, cv::Mat &shape, cv::Mat &con, cv::Mat &tri, cv::Mat &visi)
{
	int i, n = shape.rows / 2; cv::Point p1, p2; cv::Scalar c;

	//draw triangulation
	c = CV_RGB(0, 0, 0);
	for (i = 0; i < tri.rows; i++){
		if (visi.at<int>(tri.at<int>(i, 0), 0) == 0 ||
			visi.at<int>(tri.at<int>(i, 1), 0) == 0 ||
			visi.at<int>(tri.at<int>(i, 2), 0) == 0)continue;
		p1 = cv::Point(shape.at<double>(tri.at<int>(i, 0), 0),
			shape.at<double>(tri.at<int>(i, 0) + n, 0));
		p2 = cv::Point(shape.at<double>(tri.at<int>(i, 1), 0),
			shape.at<double>(tri.at<int>(i, 1) + n, 0));
		p1.y = image.size().height - p1.y;
		p2.y = image.size().height - p2.y;
		cv::line(image, p1, p2, c);
		p1 = cv::Point(shape.at<double>(tri.at<int>(i, 0), 0),
			shape.at<double>(tri.at<int>(i, 0) + n, 0));
		p2 = cv::Point(shape.at<double>(tri.at<int>(i, 2), 0),
			shape.at<double>(tri.at<int>(i, 2) + n, 0));
		p1.y = image.size().height - p1.y;
		p2.y = image.size().height - p2.y;
		cv::line(image, p1, p2, c);
		p1 = cv::Point(shape.at<double>(tri.at<int>(i, 2), 0),
			shape.at<double>(tri.at<int>(i, 2) + n, 0));
		p2 = cv::Point(shape.at<double>(tri.at<int>(i, 1), 0),
			shape.at<double>(tri.at<int>(i, 1) + n, 0));
		p1.y = image.size().height - p1.y;
		p2.y = image.size().height - p2.y;
		cv::line(image, p1, p2, c);
	}
	//draw connections
	c = CV_RGB(0, 0, 255);
	for (i = 0; i < con.cols; i++){
		if (visi.at<int>(con.at<int>(0, i), 0) == 0 ||
			visi.at<int>(con.at<int>(1, i), 0) == 0)continue;
		p1 = cv::Point(shape.at<double>(con.at<int>(0, i), 0),
			shape.at<double>(con.at<int>(0, i) + n, 0));
		p2 = cv::Point(shape.at<double>(con.at<int>(1, i), 0),
			shape.at<double>(con.at<int>(1, i) + n, 0));

		p1.y = image.size().height - p1.y;
		p2.y = image.size().height - p2.y;
		cv::line(image, p1, p2, c, 1);
	}
	//draw points
	for (i = 0; i < n; i++){
		if (visi.at<int>(i, 0) == 0)continue;
		p1 = cv::Point(shape.at<double>(i, 0), shape.at<double>(i + n, 0));
		p1.y = image.size().height - p1.y;
		c = CV_RGB(255, 0, 0); cv::circle(image, p1, 2, c);
	}return;
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
FaceTracker::FaceTracker(StateManager& stateManager)
: IState( stateManager, 0 )
{
	m_LastDelta = 0.0f;
	m_pPrimitivesDraw = nullptr;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
FaceTracker::~FaceTracker()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void FaceTracker::Enter()
{
	// vsync on/off
	SDL_GL_SetSwapInterval(0);

	m_pPrimitivesDraw = new renderer::Primitives;
	m_pPrimitivesDraw->InitialisePrimitives();

	m_BGRImage.create(640,480, CV_8UC3);

	m_Camera = new util::CameraOutput;
	glm::ivec2 videoSize(m_BGRImage.size().width, m_BGRImage.size().height);
	m_Camera->Create(videoSize, 30, 0);

	m_BGRImage.create(m_Camera->GetReadImage().size(), CV_8UC3);

	// make sure the first frame is the correct colour and orientation
	cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);
	cv::flip(m_BGRImage, m_BGRImage, 0);

	// create texture for video 
	glGenTextures(1, &m_VideoTexture);
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	GL_CHECK

	//set other tracking parameters
	wSize1[0] = 7;
	wSize2[0] = 11; wSize2[1] = 9; wSize2[2] = 7;

	model.Load(ftFile.c_str());
	tri = FACETRACKER::IO::LoadTri(triFile.c_str());
	con = FACETRACKER::IO::LoadCon(conFile.c_str());

	m_Camera->Start();

}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void FaceTracker::Exit()
{
	m_Camera->Stop();
	delete m_Camera;

	if (m_pPrimitivesDraw != nullptr)
	{
		m_pPrimitivesDraw->ShutdownPrimitives();
		delete m_pPrimitivesDraw;
		m_pPrimitivesDraw = nullptr;
	}

	m_FBOFinal.Release();

	if (m_VideoTexture != renderer::INVALID_OBJECT)
		glDeleteTextures(1, &m_VideoTexture);
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int FaceTracker::TransitionIn()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int FaceTracker::TransitionOut()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void FaceTracker::Update(float deltaTime)
{
	m_LastDelta = deltaTime;

	if (m_Camera->CanReadImage())
	{
		/*cv::GaussianBlur(m_Camera->GetReadImage(), m_BGRImage, cv::Size(3, 3), 1.5, 1.5);

		// by deafult, opencv works in BGR, so we must convert to RGB because OpenGL in windows prefer
		cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);

		// remove distorion in image
		//cv::undistort(m_BGRImage, m_RGBImage, m_CameraParameters.CameraMatrix, m_CameraParameters.Distorsion);

		cv::flip(m_BGRImage, m_BGRImage, 0);*/
		
		//cv::Mat frame = m_Camera->GetReadImage();

		//if (scale == 1)
		//	m_BGRImage = frame;
		//else 
		//	cv::resize(frame, m_BGRImage, cv::Size(scale*frame.cols, scale*frame.rows));

		cv::flip(m_Camera->GetReadImage(), m_BGRImage, 0);
		
		cv::cvtColor(m_BGRImage, m_GreyImage, CV_BGR2GRAY);

		cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);

		//track this image
		std::vector<int> wSize;
		if (failed)
			wSize = wSize2;
		else
			wSize = wSize1;

		if (model.Track(m_GreyImage, wSize, fpd, nIter, clamp, fTol, fcheck) == 0)
		{
			int idx = model._clm.GetViewIdx();
			failed = false;

			Draw(m_BGRImage, model._shape, con, tri, model._clm._visi[idx]);
		}
		else
		{
			if (show)
			{
				cv::Mat R(m_BGRImage, cvRect(0, 0, 150, 50));
				R = cv::Scalar(0, 0, 255);
			}
			model.FrameReset();
			failed = true;
		}

		m_Camera->ReadNextImage();
	}

	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_R]) {
		model.FrameReset();
	}
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void FaceTracker::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start rendering
	DrawVideoToTexture();
}

/////////////////////////////////////////////////////
/// Method: Resize
/// Params: None
///
/////////////////////////////////////////////////////
int FaceTracker::Resize(const glm::ivec2& windowSize)
{
	m_WindowDims = windowSize;

	renderer::OpenGL::GetInstance()->SetViewport(m_WindowDims.x, m_WindowDims.y);

	// create the FBO 
	m_FBOFinal.Release();

	m_FBOFinal.Initialise();
	m_FBOFinal.Create(m_WindowDims.x, m_WindowDims.y, (renderer::RTTFLAG_CREATE_COLOURBUFFER | renderer::RTTFLAG_CREATE_DEPTHBUFFER), GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, false);
	m_FBOFinal.SetClearColour(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f));

	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	// cache the uniforms and attributes
	m_TextureSampler = m_FBOFinal.GetUniformLocation(renderer::Shader::TexUnit0);
	if (m_TextureSampler != -1)
	glUniform1i(m_TextureSampler, 0);

	m_VertexAttr = m_FBOFinal.GetAttributeLocation(renderer::Shader::Vertex);

	renderer::OpenGL::GetInstance()->UseProgram(0);

	return 0;
}

/////////////////////////////////////////////////////
/// Method: DrawVideoToTexture
/// Params: None
///
/////////////////////////////////////////////////////
void FaceTracker::DrawVideoToTexture()
{
	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	renderer::OpenGL::GetInstance()->DepthMode(false, GL_ALWAYS);
	renderer::OpenGL::GetInstance()->BlendMode(false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	renderer::OpenGL::GetInstance()->SetNearFarClip(-1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(m_WindowDims.x, m_WindowDims.y, true);

	renderer::OpenGL::GetInstance()->DisableVBO();

	// bind the video texture
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);

	// no PBO
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_BGR, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_RGBImage.size().width, m_RGBImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_RGBImage.ptr(0));
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_BGRImage.size().width, m_BGRImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	// render fullscreen quad with video texture
	if (m_VertexAttr != -1)
	{
		glEnableVertexAttribArray(m_VertexAttr);
		glVertexAttribPointer(m_VertexAttr, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &vertList[0]);
	}

	GL_CHECK

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (m_VertexAttr != -1)
	{
		glDisableVertexAttribArray(m_VertexAttr);
	}

	renderer::OpenGL::GetInstance()->UseProgram(0);

	GL_CHECK
}

