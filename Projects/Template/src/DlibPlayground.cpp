
/*===================================================================
	File: DlibPlayground.cpp
=====================================================================*/

#include "DlibPlayground.h"

#ifndef DLIB_FRONTAL_FACE_DETECTOr_Hh_
#include <dlib/image_processing/frontal_face_detector.h>
#endif

#ifndef DLIB_RENDER_FACE_DeTECTIONS_H_
#include <dlib/image_processing/render_face_detections.h>
#endif

#ifndef DLIB_IMAGE_PROCESSInG_H_h_
#include <dlib/image_processing.h>
#endif

#ifndef DLIB_OPEnCV_HEADER
#include <dlib/opencv.h>
#endif

namespace
{
	glm::vec2 vertList[] =
	{
		glm::vec2(1.0f, 1.0f),
		glm::vec2(-1.0f, 1.0f),
		glm::vec2(1.0f, -1.0f),
		glm::vec2(-1.0f, -1.0f),
	};

	dlib::full_object_detection shape;
	std::vector<dlib::full_object_detection> shapes;

	dlib::frontal_face_detector detector;
	dlib::shape_predictor sp;

	#define FACE_DOWNSAMPLE_RATIO 2
	#define SKIP_FRAMES 2

	cv::Mat im_small, im_display;
	int detectCount = 0;
	std::vector<dlib::rectangle> faces;
}

void gluMultMatrixVecf(const glm::mat4& matrix, const glm::vec4& in, glm::vec4* out)
{
	int i;

	for (i = 0; i<4; i++)
	{
		out[i] =
			in[0] * matrix[0 * 4 + i] +
			in[1] * matrix[1 * 4 + i] +
			in[2] * matrix[2 * 4 + i] +
			in[3] * matrix[3 * 4 + i];
	}
}

/////////////////////////////////////////////////////
/// Function: gluProjectf
/// Params: [in]objx, [in]objy, [in]objz, [in]modelMatrix, [in]projMatrix, [in]viewport, [in/out]winx, [in/out]winy, [in/out]winz
///
/////////////////////////////////////////////////////
GLint gluProjectf(GLfloat objx, GLfloat objy, GLfloat objz,
	const glm::mat4& modelMatrix, const glm::mat4& projMatrix, const glm::ivec4 viewport,
	GLfloat *winx, GLfloat *winy, GLfloat *winz)
{
	glm::vec4 in;
	glm::vec4 out;

	in[0] = objx;
	in[1] = objy;
	in[2] = objz;
	in[3] = 1.0f;

	out = modelMatrix * in;
	//gluMultMatrixVecf(modelMatrix, in, &out);
	in = projMatrix * out;
	//gluMultMatrixVecf(projMatrix, out, &in);

	if (in[3] == 0.0f)
		return(GL_FALSE);

	in[0] /= in[3];
	in[1] /= in[3];
	in[2] /= in[3];
	// Map x, y and z to range 0-1
	in[0] = in[0] * 0.5f + 0.5f;
	in[1] = in[1] * 0.5f + 0.5f;
	in[2] = in[2] * 0.5f + 0.5f;

	// Map x,y to viewport
	in[0] = in[0] * viewport[2] + viewport[0];
	in[1] = in[1] * viewport[3] + viewport[1];


			if (winx)
				*winx = in[0];
			if (winy)
				*winy = in[1];
			if (winz)
				*winz = in[2];

			/*if (winx)
				*winx = in[0];
			if (winy)
				*winy = core::app::GetAppHeight() - in[1];
			if (winz)
				*winz = in[2];

			if (winx)
				*winx = in[1];
			if (winy)
				*winy = core::app::GetAppWidth() - in[0];
			if (winz)
				*winz = in[2];

			if (winx)
				*winx = core::app::GetAppHeight() - in[1];
			if (winy)
				*winy = in[0];
			if (winz)
				*winz = in[2];*/

	return(GL_TRUE);
}

/*
** Invert 4x4 matrix.
** Contributed by David Moore (See Mesa bug #6748)
*/
/*int gluInvertMatrixf(const glm::mat4& m, glm::mat4* invOut)
{
	glm::mat4 inv;
	float det;
	int i;

	inv[0] = m[5] * m[10] * m[15] - m[5] * m[11] * m[14] - m[9] * m[6] * m[15]
		+ m[9] * m[7] * m[14] + m[13] * m[6] * m[11] - m[13] * m[7] * m[10];
	inv[4] = -m[4] * m[10] * m[15] + m[4] * m[11] * m[14] + m[8] * m[6] * m[15]
		- m[8] * m[7] * m[14] - m[12] * m[6] * m[11] + m[12] * m[7] * m[10];
	inv[8] = m[4] * m[9] * m[15] - m[4] * m[11] * m[13] - m[8] * m[5] * m[15]
		+ m[8] * m[7] * m[13] + m[12] * m[5] * m[11] - m[12] * m[7] * m[9];
	inv[12] = -m[4] * m[9] * m[14] + m[4] * m[10] * m[13] + m[8] * m[5] * m[14]
		- m[8] * m[6] * m[13] - m[12] * m[5] * m[10] + m[12] * m[6] * m[9];
	inv[1] = -m[1] * m[10] * m[15] + m[1] * m[11] * m[14] + m[9] * m[2] * m[15]
		- m[9] * m[3] * m[14] - m[13] * m[2] * m[11] + m[13] * m[3] * m[10];
	inv[5] = m[0] * m[10] * m[15] - m[0] * m[11] * m[14] - m[8] * m[2] * m[15]
		+ m[8] * m[3] * m[14] + m[12] * m[2] * m[11] - m[12] * m[3] * m[10];
	inv[9] = -m[0] * m[9] * m[15] + m[0] * m[11] * m[13] + m[8] * m[1] * m[15]
		- m[8] * m[3] * m[13] - m[12] * m[1] * m[11] + m[12] * m[3] * m[9];
	inv[13] = m[0] * m[9] * m[14] - m[0] * m[10] * m[13] - m[8] * m[1] * m[14]
		+ m[8] * m[2] * m[13] + m[12] * m[1] * m[10] - m[12] * m[2] * m[9];
	inv[2] = m[1] * m[6] * m[15] - m[1] * m[7] * m[14] - m[5] * m[2] * m[15]
		+ m[5] * m[3] * m[14] + m[13] * m[2] * m[7] - m[13] * m[3] * m[6];
	inv[6] = -m[0] * m[6] * m[15] + m[0] * m[7] * m[14] + m[4] * m[2] * m[15]
		- m[4] * m[3] * m[14] - m[12] * m[2] * m[7] + m[12] * m[3] * m[6];
	inv[10] = m[0] * m[5] * m[15] - m[0] * m[7] * m[13] - m[4] * m[1] * m[15]
		+ m[4] * m[3] * m[13] + m[12] * m[1] * m[7] - m[12] * m[3] * m[5];
	inv[14] = -m[0] * m[5] * m[14] + m[0] * m[6] * m[13] + m[4] * m[1] * m[14]
		- m[4] * m[2] * m[13] - m[12] * m[1] * m[6] + m[12] * m[2] * m[5];
	inv[3] = -m[1] * m[6] * m[11] + m[1] * m[7] * m[10] + m[5] * m[2] * m[11]
		- m[5] * m[3] * m[10] - m[9] * m[2] * m[7] + m[9] * m[3] * m[6];
	inv[7] = m[0] * m[6] * m[11] - m[0] * m[7] * m[10] - m[4] * m[2] * m[11]
		+ m[4] * m[3] * m[10] + m[8] * m[2] * m[7] - m[8] * m[3] * m[6];
	inv[11] = -m[0] * m[5] * m[11] + m[0] * m[7] * m[9] + m[4] * m[1] * m[11]
		- m[4] * m[3] * m[9] - m[8] * m[1] * m[7] + m[8] * m[3] * m[5];
	inv[15] = m[0] * m[5] * m[10] - m[0] * m[6] * m[9] - m[4] * m[1] * m[10]
		+ m[4] * m[2] * m[9] + m[8] * m[1] * m[6] - m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];
	if (det == 0)
		return GL_FALSE;

	det = 1.0f / det;

	for (i = 0; i < 16; i++)
		invOut[i] = inv[i] * det;

	return GL_TRUE;
}*/

/////////////////////////////////////////////////////
/// Function: gluMultMatricesf
/// Params: [in]a, [in]b, [in/out]r
///
/////////////////////////////////////////////////////
/*void gluMultMatricesf(const GLfloat a[16], const GLfloat b[16], GLfloat r[16])
{
	int i, j;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			r[i * 4 + j] =
				a[i * 4 + 0] * b[0 * 4 + j] +
				a[i * 4 + 1] * b[1 * 4 + j] +
				a[i * 4 + 2] * b[2 * 4 + j] +
				a[i * 4 + 3] * b[3 * 4 + j];
		}
	}
}*/

/////////////////////////////////////////////////////
/// Function: gluUnProjectf
/// Params: [in]winx, [in]winy, [in]winz, [in]modelMatrix, [in]projMatrix, [in]viewport, [in/out]objx, [in/out]objy, [in/out]objz
///
/////////////////////////////////////////////////////
GLint gluUnProjectf(GLfloat winx, GLfloat winy, GLfloat winz,
	const glm::mat4& modelMatrix, const glm::mat4& projMatrix, const glm::ivec4& viewport,
	GLfloat *objx, GLfloat *objy, GLfloat *objz)
{
	glm::mat4 finalMatrix;
	glm::vec4 in;
	glm::vec4 out;

	finalMatrix = modelMatrix * projMatrix;
	finalMatrix = glm::inverse(finalMatrix);

	//gluMultMatricesf(modelMatrix, projMatrix, finalMatrix);
	//if (!gluInvertMatrixf(finalMatrix, finalMatrix))
	//	return(GL_FALSE);

	/*if (renderer::OpenGL::GetInstance()->GetIsRotated())
	{
		switch (renderer::OpenGL::GetInstance()->GetRotationStyle())
		{
		case renderer::VIEWROTATION_PORTRAIT_BUTTON_BOTTOM:
		{
			in[0] = winx;
			in[1] = winy;
			in[2] = winz;
		}break;
		case renderer::VIEWROTATION_PORTRAIT_BUTTON_TOP:
		{
			in[0] = core::app::GetAppWidth() - winx;
			in[1] = core::app::GetAppHeight() - winy;
			in[2] = winz;
		}break;
		case renderer::VIEWROTATION_LANDSCAPE_BUTTON_LEFT:
		{
			in[0] = core::app::GetAppWidth() - winy;
			in[1] = winx;
			in[2] = winz;
		}break;
		case renderer::VIEWROTATION_LANDSCAPE_BUTTON_RIGHT:
		{
			in[0] = winy;
			in[1] = core::app::GetAppHeight() - winx;
			in[2] = winz;
		}break;
		default:
			break;
		}
	}
	else*/
	{
		//in[0] = winx;
		//in[1] = winy;
		//in[2] = winz;

		in[0] = winx;
		in[1] = 720 - winy;
		in[2] = winz;
	}

	in[3] = 1.0f;

	/* Map x and y from window coordinates */
	in[0] = (in[0] - viewport[0]) / viewport[2];
	in[1] = (in[1] - viewport[1]) / viewport[3];

	/* Map to range -1 to 1 */
	in[0] = in[0] * 2 - 1;
	in[1] = in[1] * 2 - 1;
	in[2] = in[2] * 2 - 1;

	out = finalMatrix * in;
	//gluUtil::gluMultMatrixVecf(finalMatrix, in, out);
	if (out[3] == 0.0f)
		return(GL_FALSE);

	out[0] /= out[3];
	out[1] /= out[3];
	out[2] /= out[3];


	*objx = out[0];
	*objy = out[1];
	*objz = out[2];

	return(GL_TRUE);
}


/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
DlibPlayground::DlibPlayground(StateManager& stateManager)
: IState( stateManager, 0 )
{
	m_LastDelta = 0.0f;
	m_pPrimitivesDraw = nullptr;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
DlibPlayground::~DlibPlayground()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void DlibPlayground::Enter()
{
	// vsync on/off
	SDL_GL_SetSwapInterval(0);

	m_pPrimitivesDraw = new renderer::Primitives;
	m_pPrimitivesDraw->InitialisePrimitives();

	m_Camera = new util::CameraOutput;
	glm::ivec2 videoSize(640, 480);
	m_Camera->Create(videoSize, 30, 0);

	m_BGRImage.create(m_Camera->GetReadImage().size(), CV_8UC3);

	// make sure the first frame is the correct colour and orientation
	cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);
	cv::flip(m_BGRImage, m_BGRImage, 0);

	// We need a face detector.  We will use this to get bounding boxes for
	// each face in an image.
	detector = dlib::get_frontal_face_detector();
	// And we also need a shape_predictor.  This is the tool that will predict face
	// landmark positions given an image and face bounding box.  Here we are just
	// loading the model from the shape_predictor_68_face_landmarks.dat file you gave
	// as a command line argument.
	dlib::deserialize("data/shape_predictor_68_face_landmarks.dat") >> sp;

	// create texture for video 
	glGenTextures(1, &m_VideoTexture);
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	GL_CHECK
	
	m_Camera->Start();
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void DlibPlayground::Exit()
{
	m_Camera->Stop();
	delete m_Camera;

	if (m_pPrimitivesDraw != nullptr)
	{
		m_pPrimitivesDraw->ShutdownPrimitives();
		delete m_pPrimitivesDraw;
		m_pPrimitivesDraw = nullptr;
	}

	m_FBOFinal.Release();

	if (m_VideoTexture != renderer::INVALID_OBJECT)
		glDeleteTextures(1, &m_VideoTexture);
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int DlibPlayground::TransitionIn()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int DlibPlayground::TransitionOut()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void DlibPlayground::Update(float deltaTime)
{
	m_LastDelta = deltaTime;

	if (m_Camera->CanReadImage())
	{
		// Resize image for face detection
		cv::resize(m_Camera->GetReadImage(), im_small, cv::Size(), 1.0 / FACE_DOWNSAMPLE_RATIO, 1.0 / FACE_DOWNSAMPLE_RATIO);

		// Change to dlib's image format. No memory is copied.
		dlib::cv_image<dlib::bgr_pixel> cimg_small(im_small);
		dlib::cv_image<dlib::bgr_pixel> cimg(m_Camera->GetReadImage());

		// by deafult, opencv works in BGR, so we must convert to RGB because OpenGL in windows prefer
		cv::cvtColor(m_Camera->GetReadImage(), m_BGRImage, CV_BGR2RGB);

		cv::flip(m_BGRImage, m_BGRImage, 0);

		// Detect faces on resize image
		if (detectCount % SKIP_FRAMES == 0)
		{
			faces = detector(cimg_small);
		}
		detectCount++;

		// Find the pose of each face.
		for (unsigned long i = 0; i < faces.size(); ++i)
		{
			// Resize obtained rectangle for full resolution image. 
			dlib::rectangle r(
				(long)(faces[i].left() * FACE_DOWNSAMPLE_RATIO),
				(long)(faces[i].top() * FACE_DOWNSAMPLE_RATIO),
				(long)(faces[i].right() * FACE_DOWNSAMPLE_RATIO),
				(long)(faces[i].bottom() * FACE_DOWNSAMPLE_RATIO)
				);

			// Landmark detection on full sized image
			shape = sp(cimg, r);
			shapes.push_back(shape);
		}

		m_Camera->ReadNextImage();
	}
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void DlibPlayground::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start rendering
	DrawVideoToTexture();

	renderer::OpenGL::GetInstance()->DepthMode(true, GL_LESS);
	renderer::OpenGL::GetInstance()->SetNearFarClip(-1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(m_BGRImage.size().width, m_BGRImage.size().height, true);

    glm::mat4 mdlMat = glm::mat4(1.0f);
	renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);

	// Custom Face Render
	RenderFace(shape);

	// render sphere for positioning test
	/*{
		renderer::OpenGL::GetInstance()->SetNearFarClip(0.01f, 100.0f);
		renderer::OpenGL::GetInstance()->SetupPerspectiveView(m_WindowDims.x, m_WindowDims.y, true);
		glm::mat4 viewMat = glm::lookAt(glm::vec3(2.0f, 0.0f, 20.0f), glm::vec3(0.0f, 0.1f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
		renderer::OpenGL::GetInstance()->SetViewMatrix(viewMat);

		//renderer::OpenGL::GetInstance()->SetModelMatrix(glm::mat4(1.0f));
		static float yRotation = 0.0f;
		yRotation += 1.0f*m_LastDelta;

		mdlMat = glm::mat4(1.0f);
		renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);
		m_pPrimitivesDraw->DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
		m_pPrimitivesDraw->DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
		m_pPrimitivesDraw->DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));

		mdlMat = glm::translate(mdlMat, glm::vec3(0.0f, 0.0f, 0.0f));
		mdlMat = glm::rotate(mdlMat, yRotation, glm::vec3(0.0f, 1.0f, 0.0f));

		renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);
		m_pPrimitivesDraw->DrawSphere(1.0f, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}*/

	// jaw point
	/*if (shape.num_parts() >= 17)
	{
		glm::vec3 jawStart = glm::vec3(shape.part(0).x(), shape.part(0).y(), 1.0f);
		glm::vec3 jawEnd = glm::vec3(shape.part(16).x(), shape.part(16).y(), 1.0f);

		glm::vec3 objPos;

		gluUnProjectf(jawStart.x, jawStart.y, 0.0f, renderer::OpenGL::GetInstance()->GetProjectionMatrix(), glm::mat4(1.0f), renderer::OpenGL::GetInstance()->GetViewport(), &objPos.x, &objPos.y, &objPos.z);
		renderer::OpenGL::GetInstance()->SetModelMatrix(glm::translate(glm::mat4(1.0f), objPos));

		m_pPrimitivesDraw->DrawSphere(0.1f, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

		gluUnProjectf(jawEnd.x, jawEnd.y, 0.0f, renderer::OpenGL::GetInstance()->GetProjectionMatrix(), glm::mat4(1.0f), renderer::OpenGL::GetInstance()->GetViewport(), &objPos.x, &objPos.y, &objPos.z);
		renderer::OpenGL::GetInstance()->SetModelMatrix(glm::translate(glm::mat4(1.0f), objPos));

		m_pPrimitivesDraw->DrawSphere(0.1f, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	}*/
}

/////////////////////////////////////////////////////
/// Method: Resize
/// Params: None
///
/////////////////////////////////////////////////////
int DlibPlayground::Resize(const glm::ivec2& windowSize)
{
	m_WindowDims = windowSize;

	renderer::OpenGL::GetInstance()->SetViewport(m_WindowDims.x, m_WindowDims.y);

	// create the FBO 
	m_FBOFinal.Release();

	m_FBOFinal.Initialise();
	m_FBOFinal.Create(m_WindowDims.x, m_WindowDims.y, (renderer::RTTFLAG_CREATE_COLOURBUFFER | renderer::RTTFLAG_CREATE_DEPTHBUFFER), GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, false);
	m_FBOFinal.SetClearColour(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f));

	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	// cache the uniforms and attributes
	m_TextureSampler = m_FBOFinal.GetUniformLocation(renderer::Shader::TexUnit0);
	if (m_TextureSampler != -1)
	glUniform1i(m_TextureSampler, 0);

	m_VertexAttr = m_FBOFinal.GetAttributeLocation(renderer::Shader::Vertex);

	renderer::OpenGL::GetInstance()->UseProgram(0);

	renderer::OpenGL::GetInstance()->SetPerspective(75.0f, 4.0f / 3.0f, 1.0f, 1000.0f);

	return 0;
}

/////////////////////////////////////////////////////
/// Method: DrawVideoToTexture
/// Params: None
///
/////////////////////////////////////////////////////
void DlibPlayground::DrawVideoToTexture()
{
	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	renderer::OpenGL::GetInstance()->DepthMode(false, GL_ALWAYS);
	renderer::OpenGL::GetInstance()->BlendMode(false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	renderer::OpenGL::GetInstance()->SetNearFarClip(-1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(m_WindowDims.x, m_WindowDims.y, true);

	renderer::OpenGL::GetInstance()->DisableVBO();

	// bind the video texture
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);

	// no PBO
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_BGR, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_RGBImage.size().width, m_RGBImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_RGBImage.ptr(0));
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_BGRImage.size().width, m_BGRImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	// render fullscreen quad with video texture
	if (m_VertexAttr != -1)
	{
		glEnableVertexAttribArray(m_VertexAttr);
		glVertexAttribPointer(m_VertexAttr, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &vertList[0]);
	}

	GL_CHECK

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (m_VertexAttr != -1)
	{
		glDisableVertexAttribArray(m_VertexAttr);
	}

	renderer::OpenGL::GetInstance()->UseProgram(0);

	GL_CHECK
}

void DlibPlayground::DrawLine(const dlib::full_object_detection& d, const int start, const int end, bool isClosed)
{
	/*std::vector <cv::Point> points;
	for (int i = start; i <= end; ++i)
	{
		points.push_back(cv::Point(d.part(i).x(), d.part(i).y()));
	}
	cv::polylines(img, points, isClosed, cv::Scalar(255, 0, 0), 2, 16);*/

	std::vector <glm::vec3> points;
	for (int i = start; i <= end; ++i)
	{
		points.push_back(glm::vec3(d.part(i).x(), float(m_BGRImage.size().height) - d.part(i).y(), 0.0f));
	}

	for (std::size_t i = 0; i < points.size(); ++i)
	{
		if (i != points.size()-1 )
			m_pPrimitivesDraw->DrawLine( points[i], points[i+1], glm::vec4(0.0f, 1.0f, 0.0f, 1.0f) );
		else
		{
			if (isClosed)
			{
				m_pPrimitivesDraw->DrawLine(points[i], points[0], glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
			}
		}
	}
}

void DlibPlayground::RenderFace(const dlib::full_object_detection& d)
{
	if (d.num_parts() != 68)
	{
		//DBGLOG("\n\t Invalid inputs were given to this function.\n\t d.num_parts():  %d", d.num_parts());
		return;
	}

	DrawLine(d, 0, 16);           // Jaw line
	DrawLine(d, 17, 21);          // Left eyebrow
	DrawLine(d, 22, 26);          // Right eyebrow
	DrawLine(d, 27, 30);          // Nose bridge
	DrawLine(d, 30, 35, true);    // Lower nose
	DrawLine(d, 36, 41, true);    // Left eye
	DrawLine(d, 42, 47, true);    // Right Eye
	DrawLine(d, 48, 59, true);    // Outer lip
	DrawLine(d, 60, 67, true);    // Inner lip
}
