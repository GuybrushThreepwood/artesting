
/*===================================================================
	File: ArucoModel.cpp
=====================================================================*/

#include "Util/ModelUtils.h"

#include "ArucoModel.h"

namespace
{
	float TheMarkerSize = 0.175f;

	glm::vec2 vertList[] =
	{
		glm::vec2(1.0f, 1.0f),
		glm::vec2(-1.0f, 1.0f),
		glm::vec2(1.0f, -1.0f),
		glm::vec2(-1.0f, -1.0f),
	};

	//#define SKIP_FRAMES 1
	int detectCount = 0;
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
ArucoModel::ArucoModel(StateManager& stateManager)
: IState( stateManager, 0 )
{
	m_LastDelta = 0.0f;
	m_pPrimitivesDraw = nullptr;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
ArucoModel::~ArucoModel()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void ArucoModel::Enter()
{
	// vsync on/off
	SDL_GL_SetSwapInterval(0);

	m_pPrimitivesDraw = new renderer::Primitives;
	m_pPrimitivesDraw->InitialisePrimitives();

	m_BGRImage.create(640, 480, CV_8UC3);

	// load camera data
	m_CameraParameters.readFromXMLFile("camera_aruco.yml");
	m_CameraParameters.resize(m_BGRImage.size());

	m_Camera = new util::CameraOutput;
	glm::ivec2 videoSize(m_BGRImage.size().width, m_BGRImage.size().height);
	m_Camera->Create(videoSize, 30, 0);

	m_BGRImage.create(m_Camera->GetReadImage().size(), CV_8UC3);

	// read board configuration
	m_MMConfig.readFromFile("data/marker-map.yml");

	if (m_MMConfig.isExpressedInPixels())
		m_MMConfig = m_MMConfig.convertToMeters(TheMarkerSize);

	m_MMPoseTracker.setParams(m_CameraParameters, m_MMConfig);	

	//aruco::MarkerDetector::Params params;
	//m_MarkerDetector.setParams(params);//set the params above

	m_MarkerDetector.setThresholdMethod(aruco::MarkerDetector::ADPT_THRES);

	//NONE,HARRIS,SUBPIX,LINES
	m_MarkerDetector.setCornerRefinementMethod(aruco::MarkerDetector::LINES);

	m_MarkerDetector.setDictionary(m_MMConfig.getDictionary(), 0.5f);

	m_RGBImage.create(m_BGRImage.size(), CV_8UC3);

	// make sure the first frame is the correct colour and orientation
	cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);
	cv::flip(m_BGRImage, m_BGRImage, 0);

	// create texture for video 
	glGenTextures(1, &m_VideoTexture);
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));


	m_ModelShader.CreateFromFiles("data/shaders/21/model+colour.vert", "data/shaders/21/model+colour.frag", "");

	float bmin[3], bmax[3];
	if (!util::LoadObjAndConvert(bmin, bmax, m_DrawObjects, "data/models/The City-z.obj"))
	{
		DBGLOG("Model load failed\n");
	}

	GL_CHECK

	m_Camera->Start();
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void ArucoModel::Exit()
{
	m_Camera->Stop();
	delete m_Camera;

	if (m_pPrimitivesDraw != nullptr)
	{
		m_pPrimitivesDraw->ShutdownPrimitives();
		delete m_pPrimitivesDraw;
		m_pPrimitivesDraw = nullptr;
	}

	m_FBOFinal.Release();

	if (m_VideoTexture != renderer::INVALID_OBJECT)
		glDeleteTextures(1, &m_VideoTexture);

	m_ModelShader.Shutdown();
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int ArucoModel::TransitionIn()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int ArucoModel::TransitionOut()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void ArucoModel::Update(float deltaTime)
{
	m_LastDelta = deltaTime;

	if (m_Camera->CanReadImage())
	{
		//cv::GaussianBlur(m_ImageWrite, m_BGRImage, cv::Size(3, 3), 1.5, 1.5);

		// by deafult, opencv works in BGR, so we must convert to RGB because OpenGL in windows prefer
		cv::cvtColor(m_Camera->GetReadImage(), m_BGRImage, CV_BGR2RGB);

		// remove distorion in image
		cv::undistort(m_BGRImage, m_RGBImage, m_CameraParameters.CameraMatrix, m_CameraParameters.Distorsion);

		static float timeToRead = 0.0f;

		//detect markers
		m_Markers = m_MarkerDetector.detect(m_RGBImage);
		m_MMPoseTracker.estimatePose(m_Markers);

		util::GetCameraProjectionMatrix(m_CameraParameters, m_RGBImage.size(), m_RGBImage.size(), m_ArucoProjMat, 0.01f, 100.0f, false);

		cv::flip(m_BGRImage, m_BGRImage, 0);

		m_Camera->ReadNextImage();
	}
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void ArucoModel::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start rendering
	DrawVideoToTexture();

	renderer::OpenGL::GetInstance()->DepthMode(true, GL_LESS);

	glm::mat4 camView = glm::make_mat4(m_ArucoProjMat);
	renderer::OpenGL::GetInstance()->SetProjectionMatrix(camView);
    glm::mat4 viewMat = glm::mat4(1.0f);
	renderer::OpenGL::GetInstance()->SetViewMatrix(viewMat);

	if (!m_MMPoseTracker.getRTMatrix().empty()) {
		util::GetModelViewMatrix(m_ArucoMdlViewMat, m_MMPoseTracker.getRvec(), m_MMPoseTracker.getTvec() );

		glm::mat4 modelView = glm::make_mat4(m_ArucoMdlViewMat);
		renderer::OpenGL::GetInstance()->SetModelMatrix(modelView);

		renderer::OpenGL::GetInstance()->DepthMode(true, GL_LESS);
		renderer::OpenGL::GetInstance()->DisableTexturing();
		DrawModels(m_DrawObjects);

		renderer::OpenGL::GetInstance()->DisableVBO();
	}
}

/////////////////////////////////////////////////////
/// Method: Resize
/// Params: None
///
/////////////////////////////////////////////////////
int ArucoModel::Resize(const glm::ivec2& windowSize)
{
	m_WindowDims = windowSize;

	renderer::OpenGL::GetInstance()->SetViewport(m_WindowDims.x, m_WindowDims.y);

	// create the FBO 
	m_FBOFinal.Release();

	m_FBOFinal.Initialise();
	m_FBOFinal.Create(m_WindowDims.x, m_WindowDims.y, (renderer::RTTFLAG_CREATE_COLOURBUFFER | renderer::RTTFLAG_CREATE_DEPTHBUFFER), GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, false);
	m_FBOFinal.SetClearColour(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f));

	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	// cache the uniforms and attributes
	m_TextureSampler = m_FBOFinal.GetUniformLocation(renderer::Shader::TexUnit0);
	if (m_TextureSampler != -1)
		glUniform1i(m_TextureSampler, 0);

	m_VertexAttr = m_FBOFinal.GetAttributeLocation(renderer::Shader::Vertex);

	renderer::OpenGL::GetInstance()->UseProgram(0);

	return 0;
}

/////////////////////////////////////////////////////
/// Method: DrawVideoToTexture
/// Params: None
///
/////////////////////////////////////////////////////
void ArucoModel::DrawVideoToTexture()
{
	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	renderer::OpenGL::GetInstance()->DepthMode(false, GL_ALWAYS);
	renderer::OpenGL::GetInstance()->BlendMode(false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	renderer::OpenGL::GetInstance()->SetNearFarClip(-1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(m_WindowDims.x, m_WindowDims.y, true);

	renderer::OpenGL::GetInstance()->DisableVBO();

	// bind the video texture
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);

	// no PBO
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_BGR, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_RGBImage.size().width, m_RGBImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_RGBImage.ptr(0));
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_BGRImage.size().width, m_BGRImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	// render fullscreen quad with video texture
	if (m_VertexAttr != -1)
	{
		glEnableVertexAttribArray(m_VertexAttr);
		glVertexAttribPointer(m_VertexAttr, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &vertList[0]);
	}

	GL_CHECK

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (m_VertexAttr != -1)
	{
		glDisableVertexAttribArray(m_VertexAttr);
	}

	renderer::OpenGL::GetInstance()->UseProgram(0);

	GL_CHECK
}

/////////////////////////////////////////////////////
/// Method: DrawModels
/// Params: None
///
/////////////////////////////////////////////////////
void ArucoModel::DrawModels(const std::vector<util::DrawObject>& drawObjects)
{
	m_ModelShader.Bind();

	GLint vertexAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Vertex);
	GLint colourAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Colour);
	GLint normalAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Normal);

	GLint modelViewProjectionMatrix = m_ModelShader.GetUniformLocation(renderer::Shader::ModelViewProjectionMatrix);

	glm::mat4 projMatrix = renderer::OpenGL::GetInstance()->GetProjectionMatrix();
	glm::mat4 viewMatrix = renderer::OpenGL::GetInstance()->GetViewMatrix();
	glm::mat4 modelMatrix = renderer::OpenGL::GetInstance()->GetModelMatrix();

	static float scaleTweak = 1.0f;

	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_A]) {
		scaleTweak += 1.0f*m_LastDelta;
		//DBGLOG("%.2f\n", depthTweak);

	}
	if (state[SDL_SCANCODE_Z]) {
		scaleTweak -= 1.0f*m_LastDelta;
		//DBGLOG("%.2f\n", depthTweak);
	}
	modelMatrix = glm::scale(modelMatrix, glm::vec3(scaleTweak, scaleTweak, scaleTweak));

	glm::mat4 modelViewMatrix = viewMatrix*modelMatrix;

	if (modelViewProjectionMatrix != -1)
		glUniformMatrix4fv(modelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix*modelViewMatrix));

	for (size_t i = 0; i < drawObjects.size(); i++) {
		util::DrawObject o = drawObjects[i];
		if (o.vbo == renderer::INVALID_OBJECT) {
			continue;
		}

		glBindBuffer(GL_ARRAY_BUFFER, o.vbo);

		if (vertexAttr != -1)
		{
			glEnableVertexAttribArray(vertexAttr);
			glVertexAttribPointer(vertexAttr, 3, GL_FLOAT, GL_FALSE, 40, (const void*)0); // 3*4 12 bytes 
		}

		if (normalAttr != -1)
		{
			glEnableVertexAttribArray(normalAttr);
			glVertexAttribPointer(normalAttr, 3, GL_FLOAT, GL_FALSE, 40, (const void*)(sizeof(float) * 3)); // 3*4 12 bytes
		}

		if (colourAttr != -1)
		{
			glEnableVertexAttribArray(colourAttr);
			glVertexAttribPointer(colourAttr, 4, GL_FLOAT, GL_FALSE, 40, (const void*)(sizeof(float) * 3)); // 4*4 16 bytes
		}

		glDrawArrays(GL_TRIANGLES, 0, 3 * o.numTriangles);

		GL_CHECK
	}

	if (vertexAttr != -1)
		glDisableVertexAttribArray(vertexAttr);

	if (normalAttr != -1)
		glDisableVertexAttribArray(normalAttr);

	if (colourAttr != -1)
		glDisableVertexAttribArray(colourAttr);

	m_ModelShader.UnBind();
}

