
/*===================================================================
	File: App.cpp
=====================================================================*/

#include "Boot/Includes.h"

#include "App.h"

using namespace core::app;

App::App()
{
	InitClass();
}

App::App(int &argc, char **argv)
{
	InitClass();
	m_CmdLine.Create(argc, argv);
}

App::~App()
{

}

void App::InitClass()
{
	m_Window = nullptr;
	m_Context = nullptr;
	m_TotalFrames = 0;
	m_StartTicks = 0;
	m_FPS = 0;
	m_ElapsedTime = 0.0f;
}

void App::ToggleFullscreen() 
{
	Uint32 fullscreenFlag = SDL_WINDOW_FULLSCREEN;

	bool IsFullscreen = (SDL_GetWindowFlags(m_Window) & fullscreenFlag) != 0;

	SDL_SetWindowFullscreen(m_Window, IsFullscreen ? 0 : fullscreenFlag);

	//SDL_ShowCursor(IsFullscreen);
}

void App::UpdateFrame()
{
	Uint32 currTicks = SDL_GetTicks();

	m_ElapsedTime = static_cast<float>( (currTicks - m_LastTicks) ) / 1000.0f;
	m_FPS = (m_TotalFrames / (float)(currTicks - m_StartTicks)) * 1000.0f;

	m_LastTicks = currTicks;

	Update();
}

void App::RenderFrame()
{
	Render();
}

int App::Execute( glm::ivec2 windowSize, glm::ivec2 frameSize, bool fullscreen, bool frameLock, unsigned int lockedFramerate)
{
	std::string title;

	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		DBGLOG("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return 1;
	}
	else
	{
		//"Windows"
		//"Mac OS X"
		//"Linux"
		//"iOS"
		//"Android"
		m_Platform = std::string(SDL_GetPlatform());

		/*if (windowSize.x == -1)
		{
			int i=0;

			// Declare display mode structure to be filled in.
			SDL_DisplayMode current;

			// Get current display mode of all displays.
			for (i = 0; i < SDL_GetNumVideoDisplays(); ++i)
			{
				if (SDL_GetCurrentDisplayMode(i, &current))
				{
					windowSize.x = 1024;
				}
				else
				{
					windowSize.x = current.w;
				}
			}
		}

		if (windowSize.y == -1)
		{
			int i = 0;

			// Declare display mode structure to be filled in.
			SDL_DisplayMode current;

			// Get current display mode of all displays.
			for (i = 0; i < SDL_GetNumVideoDisplays(); ++i)
			{
				if (SDL_GetCurrentDisplayMode(i, &current))
				{
					windowSize.y = 768;
				}
				else
				{
					windowSize.y = current.h;
				}
			}
		}*/

		if (windowSize.x <= 0)
			windowSize.x = 1;

		if (windowSize.y <= 0)
			windowSize.y = 1;

		m_WindowDims = windowSize;

		/*SDL_DisplayMode target, closest;

		// Set the desired resolution, etc.
		target.w = m_WindowDims.x;
		target.h = m_WindowDims.y;
        target.format = 0;//SDL_PIXELFORMAT_RGBA8888;  // don't care
		target.refresh_rate = 0; // don't care
		target.driverdata = 0; // initialize to 0
		DBGLOG("Requesting: \t%dx%dpx @ %dhz \n", target.w, target.h, target.refresh_rate);

		// Pass the display mode structures by reference to SDL_GetClosestDisplay
		// and check whether the result is a null pointer.
		if (SDL_GetClosestDisplayMode(0, &target, &closest) == NULL)
		{
			// If the returned pointer is null, no match was found.
			DBGLOG("\nNo suitable display mode was found!\n\n");
		}
		else
		{
			// Otherwise, a display mode close to the target is available.
			// Access the SDL_DisplayMode structure to see what was received.
			DBGLOG("  Received: \t%dx%dpx @ %dhz \n", closest.w, closest.h, closest.refresh_rate);

			m_WindowDims = glm::ivec2(closest.w, closest.h);
		}*/

#ifdef USE_OPENGL21
    #ifdef USE_OPENGL
		// Use OpenGL 2.1
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    #elif USE_OPENGL_ES
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
        
        if (m_Platform == "iOS")
        {
            SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
            SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 6);
            SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
            SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        
            SDL_GL_SetAttribute(SDL_GL_RETAINED_BACKING, 1);
            SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
        }
    #endif
#elif USE_OPENGL41
	#ifdef USE_OPENGL
		// Use OpenGL 4.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	#elif USE_OPENGL_ES
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
	#endif
#endif
		// Create window
		Uint32 flags = SDL_WINDOW_OPENGL /*| SDL_WINDOW_RESIZABLE*/ | SDL_WINDOW_ALLOW_HIGHDPI;
        
		//if (fullscreen)
		//	flags |= SDL_WINDOW_FULLSCREEN;

		if (m_Platform == "iOS")
			flags |=  SDL_WINDOW_BORDERLESS;

		m_Window = SDL_CreateWindow("SDL Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_WindowDims.x, m_WindowDims.y, flags);
		if (m_Window == nullptr)
		{
			DBGLOG("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return 1;
		}
		else
		{
			SDL_GetWindowSize( m_Window, &m_WindowDims.x, &m_WindowDims.y );

			m_Context = SDL_GL_CreateContext(m_Window);
			if (m_Context == nullptr)
			{
				DBGLOG("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				return 1;
			}
			else
			{
                int w,h;
                SDL_GL_GetDrawableSize(m_Window,&w,&h);
                DBGLOG( "Draw size %dx%d\n", w, h );
                
                SDL_GetWindowMinimumSize(m_Window,&w,&h);
                DBGLOG( "Min size %dx%d\n", w, h );

                SDL_GetWindowMaximumSize(m_Window,&w,&h);
                DBGLOG( "Max size %dx%d\n", w, h );
                
                // initialise OpenGL
				renderer::OpenGL::Initialise();

				// some default GL values
				renderer::OpenGL::GetInstance()->Init();
                
				if (frameSize.x < 0 ||
					frameSize.x > renderer::OpenGL::GetInstance()->GetMaxRenderBufferSize())
					frameSize.x = m_WindowDims.x;

				if (frameSize.y < 0 || 
					frameSize.y > renderer::OpenGL::GetInstance()->GetMaxRenderBufferSize())
					frameSize.y = m_WindowDims.y;

				m_FrameDims = frameSize;

				renderer::OpenGL::GetInstance()->SetupPerspectiveView(m_FrameDims.x, m_FrameDims.y);
				renderer::OpenGL::GetInstance()->SetNearFarClip(1.0f, 10000.0f);
				renderer::OpenGL::GetInstance()->ClearColour(0.0f, 0.282f, 0.415f, 1.0f);

				Initialise();
			}

			// Main loop flag
			bool quit = false;

			// Event handler
			SDL_Event e;

			m_TotalFrames = 0;
			m_LastTicks = m_StartTicks = SDL_GetTicks();

			//While application is running
			while (!quit)
			{
				while (SDL_PollEvent(&e) != 0)
                //if (SDL_WaitEvent(&e) != 0)
				{
					switch (e.type)
					{
						//User requests quit 
						case SDL_QUIT:
						{
							quit = true;
						}break;
						case SDL_KEYDOWN:
						{
							// key press 
							switch (e.key.keysym.sym)
							{
								case SDLK_RETURN:
								{
									ToggleFullscreen();
								}break;
								case SDLK_ESCAPE:
								{
									quit = true;
								}break;

								default:
									break;
							}
						}break;
						case SDL_KEYUP:
						{

						}break;

						default:
							break;
					}
				}

                Uint32 frameStartTime = SDL_GetTicks();
                
				UpdateFrame();

                SDL_GL_MakeCurrent(m_Window, m_Context);
                renderer::OpenGL::GetInstance()->BindDefaults();
                
				RenderFrame();

				GL_CHECK

                SDL_GL_MakeCurrent(m_Window, m_Context);
				SDL_GL_SwapWindow(m_Window);

				if (frameLock)
				{
					Uint32 frameTick = (SDL_GetTicks() - frameStartTime);

					if (frameTick < (1000 / lockedFramerate))
					{
						Uint32 delayTime = (1000 / lockedFramerate) - frameTick;

						SDL_Delay(delayTime);
					}
				}
                else
                    SDL_Delay(1);
                
				title = "FPS: " + std::to_string(m_FPS) + " | FrameTime: " + std::to_string(m_ElapsedTime);
				SDL_SetWindowTitle(m_Window, title.c_str());
				m_TotalFrames++;
			}
		}
	}

	Shutdown();

	// release OpenGL
	renderer::OpenGL::Shutdown();

	SDL_GL_DeleteContext(m_Context);

	// Destroy window
	SDL_DestroyWindow(m_Window);

	// Quit SDL subsystems
	SDL_Quit();

	return 0;
}

void App::ResizeWindow(const glm::ivec2& windowSize)
{
    SDL_GL_MakeCurrent(m_Window, m_Context);
    
	m_WindowDims = windowSize;

	if (m_WindowDims.x <= 0)
		m_WindowDims.x = 1;

	if (m_WindowDims.y <= 0)
		m_WindowDims.y = 1;

    if (m_Platform == "iOS")
        SDL_SetWindowSize(m_Window, m_WindowDims.x-1, m_WindowDims.y);
    else
        SDL_SetWindowSize(m_Window, m_WindowDims.x, m_WindowDims.y);
    
	SDL_SetWindowPosition(m_Window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);

	SDL_GetWindowSize(m_Window, &m_WindowDims.x, &m_WindowDims.y);

	Resize(m_WindowDims);
}
