
/*===================================================================
	File: CameraOutputTest.h
=====================================================================*/

#ifndef __CAMERAOUTPUTTEST_H__
#define __CAMERAOUTPUTTEST_H__

#include "Boot/Includes.h"

// aruco 
#include "aruco.h"

class CameraOutputTest : public IState
{
	public:
		CameraOutputTest(StateManager& stateManager);
		virtual ~CameraOutputTest();

		virtual void Enter();
		virtual void Exit();

		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Render();
		virtual int Resize(const glm::ivec2& windowSize);

		void DrawVideoToTexture();

	private:
		float m_LastDelta;

		cv::Mat m_BGRImage, m_RGBImage, m_ImageWrite;

        util::CameraOutput* m_Camera;
		GLuint m_VideoTexture;

		renderer::Framebuffer m_FBOFinal;
		GLint m_TextureSampler;
		GLint m_VertexAttr;
};

#endif // __CAMERAOUTPUTTEST_H__

