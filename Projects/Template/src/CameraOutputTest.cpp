
/*===================================================================
	File: CameraOutputTest.cpp
=====================================================================*/

#include "CameraOutputTest.h"

namespace
{
	float TheMarkerSize = 0.175f;

	glm::vec2 vertList[] =
	{
		glm::vec2(1.0f, 1.0f),
		glm::vec2(-1.0f, 1.0f),
		glm::vec2(1.0f, -1.0f),
		glm::vec2(-1.0f, -1.0f),
	};
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
CameraOutputTest::CameraOutputTest(StateManager& stateManager)
: IState( stateManager, 0 )
{
	m_LastDelta = 0.0f;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
CameraOutputTest::~CameraOutputTest()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void CameraOutputTest::Enter()
{
	// vsync on/off
	SDL_GL_SetSwapInterval(0);

    m_Camera = new util::CameraOutput;
    glm::ivec2 videoSize( 640, 480 );
    m_Camera->Create(videoSize, 30, 0);
	
    m_BGRImage.create(m_Camera->GetReadImage().size(), CV_8UC3);
    
	m_RGBImage.create(m_BGRImage.size(), CV_8UC3);

	// make sure the first frame is the correct colour and orientation
	cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);
	cv::flip(m_BGRImage, m_BGRImage, 0);

	// create texture for video 
	glGenTextures(1, &m_VideoTexture);
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	GL_CHECK
    
    m_Camera->Start();
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void CameraOutputTest::Exit()
{
    m_Camera->Stop();
    delete m_Camera;
    
	m_FBOFinal.Release();

	if (m_VideoTexture != renderer::INVALID_OBJECT)
		glDeleteTextures(1, &m_VideoTexture);
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int CameraOutputTest::TransitionIn()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int CameraOutputTest::TransitionOut()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void CameraOutputTest::Update(float deltaTime)
{
	m_LastDelta = deltaTime;

	static float timeToRead = 0.0f;

	timeToRead += m_LastDelta;
	//if (timeToRead > 0.15f)
	{
        if (m_Camera->CanReadImage())
        {
            cv::GaussianBlur(m_Camera->GetReadImage(), m_BGRImage, cv::Size(3, 3), 1.5, 1.5);

            // by deafult, opencv works in BGR, so we must convert to RGB because OpenGL in windows prefer
            cv::cvtColor(m_BGRImage, m_BGRImage, CV_BGR2RGB);

            //cv::flip(m_BGRImage, m_BGRImage, 0);

            m_Camera->ReadNextImage();
        }

		timeToRead = 0.0f;
	}
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void CameraOutputTest::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start rendering
	DrawVideoToTexture();
}

/////////////////////////////////////////////////////
/// Method: Resize
/// Params: None
///
/////////////////////////////////////////////////////
int CameraOutputTest::Resize(const glm::ivec2& windowSize)
{
	m_WindowDims = windowSize;

	renderer::OpenGL::GetInstance()->SetViewport(m_WindowDims.x, m_WindowDims.y);

	// create the FBO 
	m_FBOFinal.Release();

	m_FBOFinal.Initialise();
	m_FBOFinal.Create(m_WindowDims.x, m_WindowDims.y, (renderer::RTTFLAG_CREATE_COLOURBUFFER | renderer::RTTFLAG_CREATE_DEPTHBUFFER), GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, false);
	m_FBOFinal.SetClearColour(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f));

	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	// cache the uniforms and attributes
	m_TextureSampler = m_FBOFinal.GetUniformLocation(renderer::Shader::TexUnit0);
	if (m_TextureSampler != -1)
	glUniform1i(m_TextureSampler, 0);

	m_VertexAttr = m_FBOFinal.GetAttributeLocation(renderer::Shader::Vertex);

	renderer::OpenGL::GetInstance()->UseProgram(0);

	return 0;
}

/////////////////////////////////////////////////////
/// Method: DrawVideoToTexture
/// Params: None
///
/////////////////////////////////////////////////////
void CameraOutputTest::DrawVideoToTexture()
{
	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	renderer::OpenGL::GetInstance()->DepthMode(false, GL_ALWAYS);
	renderer::OpenGL::GetInstance()->BlendMode(false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	renderer::OpenGL::GetInstance()->SetNearFarClip(-1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(m_WindowDims.x, m_WindowDims.y, true);

	renderer::OpenGL::GetInstance()->DisableVBO();

	// bind the video texture
	renderer::OpenGL::GetInstance()->BindTexture(m_VideoTexture);

	// no PBO
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_BGRImage.size().width, m_BGRImage.size().height, 0, GL_BGR, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));
	//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_RGBImage.size().width, m_RGBImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_RGBImage.ptr(0));
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_BGRImage.size().width, m_BGRImage.size().height, GL_RGB, GL_UNSIGNED_BYTE, m_BGRImage.ptr(0));

	// render fullscreen quad with video texture
	if (m_VertexAttr != -1)
	{
		glEnableVertexAttribArray(m_VertexAttr);
		glVertexAttribPointer(m_VertexAttr, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &vertList[0]);
	}

	GL_CHECK

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (m_VertexAttr != -1)
	{
		glDisableVertexAttribArray(m_VertexAttr);
	}

	renderer::OpenGL::GetInstance()->UseProgram(0);

	GL_CHECK
}

