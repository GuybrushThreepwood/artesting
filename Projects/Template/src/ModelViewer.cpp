
/*===================================================================
	File: ModelViewer.cpp
=====================================================================*/

#include "Util/ModelUtils.h"

#include "ModelViewer.h"

namespace
{
	glm::vec2 vertList[] =
	{
		glm::vec2(1.0f, 1.0f),
		glm::vec2(-1.0f, 1.0f),
		glm::vec2(1.0f, -1.0f),
		glm::vec2(-1.0f, -1.0f),
	};
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
ModelViewer::ModelViewer(StateManager& stateManager)
: IState( stateManager, 0 )
{
	m_LastDelta = 0.0f;
	m_pPrimitivesDraw = nullptr;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
ModelViewer::~ModelViewer()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void ModelViewer::Enter()
{
	// vsync on/off
	SDL_GL_SetSwapInterval(0);
	glClearColor(0.7f, 0.7f, 0.7f, 1.0f);

	m_pPrimitivesDraw = new renderer::Primitives;
	m_pPrimitivesDraw->InitialisePrimitives();

	m_ModelShader.CreateFromFiles("data/shaders/21/model+colour.vert", "data/shaders/21/model+colour.frag", "" );

	float bmin[3], bmax[3];
	if (!util::LoadObjAndConvert(bmin, bmax, m_DrawObjects, "data/models/head-obj.obj"))
	{
		DBGLOG("Model load failed\n");
	}
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void ModelViewer::Exit()
{
	if (m_pPrimitivesDraw != nullptr)
	{
		m_pPrimitivesDraw->ShutdownPrimitives();
		delete m_pPrimitivesDraw;
		m_pPrimitivesDraw = nullptr;
	}

	m_ModelShader.Shutdown();
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int ModelViewer::TransitionIn()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int ModelViewer::TransitionOut()
{
	return(0);
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void ModelViewer::Update(float deltaTime)
{
	m_LastDelta = deltaTime;
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void ModelViewer::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
	static float yRotation = 0.0f;
	yRotation += 0.5f*m_LastDelta;

	glm::mat4 mdlMat = glm::mat4(1.0f);

	renderer::OpenGL::GetInstance()->SetNearFarClip(1.0f, 100000.0f);
	renderer::OpenGL::GetInstance()->SetupPerspectiveView(m_WindowDims.x, m_WindowDims.y, true);
    glm::mat4 viewMat = glm::lookAt(glm::vec3(2.0f, 10.0f, 1000.0f), glm::vec3(0.0f, 0.1f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	renderer::OpenGL::GetInstance()->SetViewMatrix(viewMat);

	mdlMat = glm::translate(mdlMat, glm::vec3(0.0f, 5.0f, 0.0f));
	mdlMat = glm::rotate(mdlMat, yRotation, glm::vec3(0.0f, 1.0f, 0.0f));

	renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);

	renderer::OpenGL::GetInstance()->DepthMode(true, GL_LESS);
	renderer::OpenGL::GetInstance()->DisableTexturing();
	DrawModels(m_DrawObjects);

	renderer::OpenGL::GetInstance()->DisableVBO();

	// draw axis
    mdlMat = glm::mat4(1.0f);
	renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);
	m_pPrimitivesDraw->DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	m_pPrimitivesDraw->DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	m_pPrimitivesDraw->DrawLine(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));

	mdlMat = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	mdlMat = glm::rotate(mdlMat, yRotation, glm::vec3(0.0f, 1.0f, 0.0f));

	renderer::OpenGL::GetInstance()->SetModelMatrix(mdlMat);
	m_pPrimitivesDraw->DrawSphere(1.0f, glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));

}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
int ModelViewer::Resize(const glm::ivec2& windowSize)
{
	m_WindowDims = windowSize;

	renderer::OpenGL::GetInstance()->SetViewport(m_WindowDims.x, m_WindowDims.y);

	// create the FBO 
	m_FBOFinal.Release();

	m_FBOFinal.Initialise();
	m_FBOFinal.Create(m_WindowDims.x, m_WindowDims.y, (renderer::RTTFLAG_CREATE_COLOURBUFFER | renderer::RTTFLAG_CREATE_DEPTHBUFFER), GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, false);
	m_FBOFinal.SetClearColour(glm::vec4(0.2f, 0.2f, 0.2f, 1.0f));

	renderer::OpenGL::GetInstance()->UseProgram(m_FBOFinal.GetProgram());

	// cache the uniforms and attributes
	m_TextureSampler = m_FBOFinal.GetUniformLocation(renderer::Shader::TexUnit0);
	if (m_TextureSampler != -1)
	glUniform1i(m_TextureSampler, 0);

	m_VertexAttr = m_FBOFinal.GetAttributeLocation(renderer::Shader::Vertex);

	renderer::OpenGL::GetInstance()->UseProgram(0);

	renderer::OpenGL::GetInstance()->SetPerspective(75.0f, (float)m_WindowDims.x / (float)m_WindowDims.y, 1.0f, 1000.0f);

	return 0;
}

/////////////////////////////////////////////////////
/// Method: DrawModels
/// Params: None
///
/////////////////////////////////////////////////////
void ModelViewer::DrawModels(const std::vector<util::DrawObject>& drawObjects)
{
	m_ModelShader.Bind();

	GLint vertexAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Vertex);
	GLint colourAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Colour);
	GLint normalAttr = m_ModelShader.GetAttributeLocation(renderer::Shader::Normal);

	GLint modelViewProjectionMatrix = m_ModelShader.GetUniformLocation(renderer::Shader::ModelViewProjectionMatrix);

	glm::mat4 projMatrix = renderer::OpenGL::GetInstance()->GetProjectionMatrix();
	glm::mat4 viewMatrix = renderer::OpenGL::GetInstance()->GetViewMatrix();
	glm::mat4 modelMatrix = renderer::OpenGL::GetInstance()->GetModelMatrix();

	glm::mat4 modelViewMatrix = viewMatrix*modelMatrix;

	if (modelViewProjectionMatrix != -1)
		glUniformMatrix4fv(modelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix*modelViewMatrix));

	for (size_t i = 0; i < drawObjects.size(); i++) {
		util::DrawObject o = drawObjects[i];
		if (o.vbo == renderer::INVALID_OBJECT ) {
			continue;
		}

		glBindBuffer(GL_ARRAY_BUFFER, o.vbo);

		if (vertexAttr != -1)
		{
			glEnableVertexAttribArray(vertexAttr);
			glVertexAttribPointer(vertexAttr, 3, GL_FLOAT, GL_FALSE, 40, (const void*)0); // 3*4 12 bytes 
		}

		if (normalAttr != -1)
		{
			glEnableVertexAttribArray(normalAttr);
			glVertexAttribPointer(normalAttr, 3, GL_FLOAT, GL_FALSE, 40, (const void*)(sizeof(float) * 3)); // 3*4 12 bytes
		}

		if (colourAttr != -1)
		{
			glEnableVertexAttribArray(colourAttr);
			glVertexAttribPointer(colourAttr, 4, GL_FLOAT, GL_FALSE, 40, (const void*)(sizeof(float) * 3)); // 4*4 16 bytes
		}

		glDrawArrays(GL_TRIANGLES, 0, 3 * o.numTriangles);

		GL_CHECK
	}

	if (vertexAttr != -1)
		glDisableVertexAttribArray(vertexAttr);

	if (normalAttr != -1)
		glDisableVertexAttribArray(normalAttr);

	if (colourAttr != -1)
		glDisableVertexAttribArray(colourAttr);

	m_ModelShader.UnBind();
}
