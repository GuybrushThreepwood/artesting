
/*===================================================================
	File: DlibPlayground.h
=====================================================================*/

#ifndef __DLIBPLAYGROUND_H__
#define __DLIBPLAYGROUND_H__

#include "Boot/Includes.h"

namespace dlib { class full_object_detection; }

class DlibPlayground : public IState
{
	public:
		DlibPlayground(StateManager& stateManager);
		virtual ~DlibPlayground();

		virtual void Enter();
		virtual void Exit();

		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Render();
		virtual int Resize(const glm::ivec2& windowSize);

	private:
		void DrawVideoToTexture();

		void DrawLine(const dlib::full_object_detection& d, const int start, const int end, bool isClosed = false);
		void RenderFace(const dlib::full_object_detection& d);

	private:
		float m_LastDelta;

		util::CameraOutput* m_Camera;
		cv::Mat m_BGRImage;

		GLuint m_VideoTexture;

		renderer::Framebuffer m_FBOFinal;
		GLint m_TextureSampler;
		GLint m_VertexAttr;

		renderer::Primitives* m_pPrimitivesDraw;
};

#endif // __DLIBPLAYGROUND_H__

