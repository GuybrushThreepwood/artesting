
/*===================================================================
	File: FaceTracker.h
=====================================================================*/

#ifndef __FACETRACKER_H__
#define __FACETRACKER_H__

#include "Boot/Includes.h"

class FaceTracker : public IState
{
	public:
		FaceTracker(StateManager& stateManager);
		virtual ~FaceTracker();

		virtual void Enter();
		virtual void Exit();

		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Render();
		virtual int Resize(const glm::ivec2& windowSize);

	private:
		void DrawVideoToTexture();

	private:
		float m_LastDelta;

		util::CameraOutput* m_Camera;
		cv::Mat m_BGRImage, m_GreyImage;

		GLuint m_VideoTexture;

		renderer::Framebuffer m_FBOFinal;
		GLint m_TextureSampler;
		GLint m_VertexAttr;

		renderer::Primitives* m_pPrimitivesDraw;
};

#endif // __ARUCOPLAYGROUND_H__

