
/*===================================================================
	File: ModelViewer.h
=====================================================================*/

#ifndef __MODELVIEWER_H__
#define __MODELVIEWER_H__

#include "Boot/Includes.h"

namespace util { struct DrawObject; }

class ModelViewer : public IState
{
	public:
		ModelViewer(StateManager& stateManager);
		virtual ~ModelViewer();

		virtual void Enter();
		virtual void Exit();

		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Render();
		virtual int Resize(const glm::ivec2& windowSize);

	private:
		void DrawModels(const std::vector<util::DrawObject>& drawObjects);

	private:
		float m_LastDelta;

		renderer::Framebuffer m_FBOFinal;
		GLint m_TextureSampler;
		GLint m_VertexAttr;

		renderer::Primitives* m_pPrimitivesDraw;

		std::vector<util::DrawObject> m_DrawObjects;
		renderer::Shader m_ModelShader;
};

#endif // __MODELVIEWER_H__

