
/*===================================================================
	File: ArucoModel.h
=====================================================================*/

#ifndef __ARUCOMODEL_H__
#define __ARUCOMODEL_H__

#include "Boot/Includes.h"

// aruco 
#include "aruco.h"

namespace util { struct DrawObject; }

class ArucoModel : public IState
{
	public:
		ArucoModel(StateManager& stateManager);
		virtual ~ArucoModel();

		virtual void Enter();
		virtual void Exit();

		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Render();
		virtual int Resize(const glm::ivec2& windowSize);

	private:
		void DrawVideoToTexture();

		void DrawModels(const std::vector<util::DrawObject>& drawObjects);

	private:
		float m_LastDelta;

		util::CameraOutput* m_Camera;
		cv::Mat m_BGRImage, m_RGBImage;

		aruco::CameraParameters m_CameraParameters;

		aruco::MarkerMap m_MMConfig;
		aruco::MarkerDetector m_MarkerDetector;
		aruco::MarkerMapPoseTracker m_MMPoseTracker;
		std::vector<aruco::Marker> m_Markers;

		GLuint m_VideoTexture;

		renderer::Framebuffer m_FBOFinal;
		GLint m_TextureSampler;
		GLint m_VertexAttr;

		bool m_HasLastKnown;
		float m_ArucoProjMat[16];
		float m_ArucoMdlViewMat[16];
		renderer::Primitives* m_pPrimitivesDraw;

		std::vector<util::DrawObject> m_DrawObjects;
		renderer::Shader m_ModelShader;
};

#endif // __ARUCOMODEL_H__

