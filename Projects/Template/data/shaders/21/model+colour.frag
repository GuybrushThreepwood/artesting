
// basic_1tex.frag
//
// Simple 1 texture no lighting

//precision mediump float;

//uniform sampler2D texUnit0;

//varying vec3 v;
//varying vec2 uv0;
varying vec4 colour0;

void main()
{
	gl_FragColor = colour0;//texture2D(texUnit0, uv0.st);
}
