
// fullscreen.frag
//
// Render a fullscreen quad with 1 texture

//precision lowp float;

uniform sampler2D ogl_TexUnit0;

varying vec2 uv0;

void main() 
{
   vec4 colour = texture2D(ogl_TexUnit0,uv0.st);
   
   gl_FragColor.a = 1.0;
   gl_FragColor = colour;
}
