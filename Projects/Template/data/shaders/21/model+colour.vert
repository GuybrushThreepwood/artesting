
// basic_1tex.vert
//
// Simple 1 texture no lighting

//precision mediump float;

attribute vec3 base_v;
attribute vec2 base_uv0;
attribute vec4 base_c;
attribute vec3 base_n;

varying vec2 uv0;
varying vec4 colour0;

uniform mat4 ogl_ModelViewProjectionMatrix;

void main()
{
	// build the texture coord
	uv0 = base_uv0;
	// build the colour
	colour0 = base_c;
	
	// build the vertex
	vec4 vInVertex = ogl_ModelViewProjectionMatrix * vec4(base_v, 1.0);
	
	gl_Position = vInVertex;
} 
