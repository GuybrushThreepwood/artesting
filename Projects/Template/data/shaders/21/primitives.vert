
// primitives.frag
//
// Render a vertices with colour

//precision lowp float;

attribute vec3 base_v;

varying vec4 colour0;
		
uniform mat4 ogl_ModelViewProjectionMatrix;
uniform vec4 ogl_VertexColour;

void main()
{	
	// colour
	colour0 = ogl_VertexColour;

	// build the vertex
	vec4 vInVertex = ogl_ModelViewProjectionMatrix * vec4(base_v, 1.0);
	
	gl_Position = vInVertex;
} 
