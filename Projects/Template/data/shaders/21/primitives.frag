
// primitives.frag
//
// Render a vertices with colour

//precision lowp float;

varying vec4 colour0;

void main()
{
	gl_FragColor = colour0;
}
