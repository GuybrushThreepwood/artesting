
// fullscreen.vert
//
// Render a fullscreen quad with 1 texture

//precision lowp float;

attribute vec2 base_v;

varying vec2 uv0;

void main()
{
   uv0.xy = base_v.xy*0.5+0.5; // scale vertex attribute to [0-1] range
   
   gl_Position = vec4(base_v.xy,0.0,1.0);
} 
